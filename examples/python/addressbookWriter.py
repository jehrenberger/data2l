import DOM
import Reader
import Writer
import Definition 

#helper function for adding to array
def add(array, Name, Street):
    array.append( ( 0 , #offset will be calculated during the save
                    ( len(Name), Name,
                      len(Street), Street )
                     ) )

compiled_def = Definition.load_and_compile("../file_defs/AddressBook.py")

# Create empty DOM structure of the address book file format.
addressBook = DOM.DOMStruct( Reader.NullReader(), compiled_def.dep_root , None)

#fill the DOM structure with some values
add(addressBook.AddressArray, 'Vaclav',   'Hradni 14, Prague, CZ')
add(addressBook.AddressArray, 'Erwin',    'Oxford Street 15, London, GB')
add(addressBook.AddressArray, 'Gertrude', 'Schwarzstr. 16, Heilbronn, GE')


# Write the populated DOM to file
w = Writer.WriterA("addressbook.dat")
addressBook.save(w)
w.close()
