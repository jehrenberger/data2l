from file_defs.prelude import *

bmp_header = Struct('bmp_header',
    ["""Demo description for .BMP header parsing

source  http://en.wikipedia.org/wiki/BMP_file_format"""],
    'the magic number used to identify the BMP file: 0x42 0x4D (Hex code points for B and M)',
    Basic('Magic', String() ),
        Dependency(size = '2', 
),
        Assertion(name = 'Header always have to contain BM string', code = """strindex(Magic,\"BM\") == 0 
/* string BM starts on */"""),
    'the size of the BMP file in bytes',
    Basic('FileSize', DWord()),
    'reserved , shell be 0',
    Basic('Reserved', DWord()),
    'Offset of bitmap data',
    Basic('DataOffsStart', DWord()),
    'size of BITMAPINFOHEADER structure, must be 40',
    Basic('SizeBITMAPINFOHEADER', DWord()),
    'image width in pixels',
    Basic('Width', DWord()),
    'image height in pixels',
    Basic('Height', DWord()),
    'number of planes in the image, must be 1',
    Basic('NumPlanes', Word()),
    Basic('BitsPerPixel', Word()),
    'compression type (0=none, 1=RLE-8, 2=RLE-4)',
    Basic('CompressionType', DWord()),
    'size of image data in bytes (including padding)',
    Basic('ImageDataSize', DWord()),
    Basic('HorizontalRes', DWord()),
    Basic('VerticalRec', DWord()),
    Basic('NumColors', DWord()),
    Basic('NumImportantColors', DWord()),
)


definition = Definition(bmp_header)
