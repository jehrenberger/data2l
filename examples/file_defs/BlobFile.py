from file_defs.prelude import *

Header = Struct('Header',
    ['File header'],
    'File format version',
    Basic('Version', Word()),
        Dependency(offset = '0', 
),
    'The number of stored objects',
    Basic('NumberOfObjects', Word()),
        Dependency(offset = '2', 
),
)

ObjectRec = Struct('ObjectRec',
    Basic('ObjectName', String() ),
        Dependency(size = '255', 
),
    Attrib('ObjectOffset', DWord()),
        Dependency(init = '0', 
),
    Basic('ObjectSize', DWord()),
)

Object = Struct('Object',
)

ObjectArray = Array('ObjectArray',
    ObjectRec,
        Dependency(in_memory = True, 
),
    Object,
)

BlobFile = Struct('BlobFile',
    ['File for storing file objects'],
    'File identifier',
    Basic('MagicID', String() ),
        Dependency(size = '10', 
),
    Header,
        Dependency(offset = '15', 
),
    ObjectArray,
)


definition = Definition(BlobFile)
