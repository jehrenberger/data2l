import types
import BasicTypes as BT
import DataTypes as DT
import ValueExtractors as VE
from Attribute import Attribute
from exprs.ExprVE import *
from Traversal import Traversable
from Utility import impossible, xassert, pure_virtual
import Reader



__all__ = """\
DOMVisitor
DOMElement
DOMVoid
DOMBasic
DOMBasicArray
DOMStruct
DOMArray
DOMAlternative
DOMException
"""


## Interface of DOM visitors.
class DOMVisitor(object):
    def VisitDOMVoid(self, dom):
        pass

    def VisitDOMBasic(self, dom):
        pass

    def VisitDOMStruct(self, dom):
        pass

    def VisitDOMArray(self, dom):
        pass

    def VisitDOMAlternative(self, dom):
        pass


## Exception thrown on call to pure virtual function.
class DOMException(Exception):
    def __init__(self, reason):
        Exception.__init__(self, reason)
        
def RaiseMoreInfoAboutProblem(exception,dom):
    description = exception.message +"\n\nIn Path:\n  "
    path = ""
    while dom is not None:
        if isinstance(dom,DOMArray):
            path = "%s[%d]" % (dom.getDependency().getName(),dom.getCurrentElement())  + "." + path
        else:
            path = dom.getDependency().getName() + "." + path
        dom = dom.parentDOM()
    
    raise DOMException(description + path + "\n\n" )
    
    

## Base class for all DOM elements.
class DOMElement(Traversable):
    def __init__(self, reader, dep, own_index, parent):
        ## Instance of Reader class that is the source of data for DOMElement.
        self.reader = reader
        ## DataElement that DOM represents.
        self.dep = dep
        ## Parent DOM.
        self.parent = parent
        ## Own index/position with respect to parent.
        xassert(own_index is None or own_index >= 0)
        self.own_index = own_index
        ## Offset of start of data for this element.
        self.akt_offs = None
        ## Currently selected element.
        self.current_elem = None

    def __str__(self):
        return "DOM:"+str(self.dep.getName())

    ## This method just seeks to right offset if it is
    # needed. Descendant classes will probably overload this method to
    # load their contents from reader.
    def Init(self):
        if self.dep.offset is not None:
            self.akt_offs = self.dep.offset.getDOMValue(self)
            self.reader.seek( self.akt_offs )
        else:
            self.akt_offs = self.reader.tell()
#        print "offsetof("+str(self)+"): "+str(self.akt_offs)

    def save(self, writer):
        self.akt_offs = writer.tell();

    ## Internal only method.
    def createDOMElement(self, dep, its_index):
        dom = None

        if isinstance(dep.getType(), DT.Struct):
            dom = DOMStruct(self.reader, dep, its_index, self)
        elif isinstance(dep.getType(), DT.Basic):
            if isinstance(dep.getType().basic_type, BT.BasicArrayType):
                dom = DOMBasicArray(self.reader, dep, its_index, self)
            else:
                dom = DOMBasic(self.reader, dep, its_index, self)
        elif isinstance(dep.getType(), DT.Array):
            dom = DOMArray(self.reader, dep, its_index, self)
        elif isinstance(dep.getType(), DT.Alternative):
            dom = DOMAlternative(self.reader, dep, its_index, self)
        elif isinstance(dep.getType(), DT.Void):
            dom = DOMVoid(self.reader, dep, its_index, self)

        xassert(dom is not None)

        if self.__dict__.has_key(dep.getName()):
            if self.__dict__[dep.getName()].__class__ != dom.__class__:
        	  raise DOMException("Atribute name \""+dep.getName()+"\" is used in internal python representation.")
        setattr(self, dep.getName(), dom )
        
        return dom


    ## \deprecated Changes currently selected element.
    def setCurrentElement(self, index):
        self.current_elem = index

    ## \Returns currently selected element.
    def getCurrentElement(self):
        return self.current_elem

    ##
    # \returns a position of a DOM with respect to its parent.  For
    # DOM as field of structure, it returns index of the field number
    # within structure.  For DOM as element of array, it is index of
    # element within array.  For alternative, it is alternative's
    # index as used in selector.
    def ownIndex(self):
        return self.own_index

    ## Sets own index of DOMElement with respect to parent.
    # \sa ownIndex()
    def setOwnIndex(self, own_index):
        self.own_index = own_index

    ## \Returns offset of start of data for this element.
    def aktOffset(self):
        return self.akt_offs

    ## \Returns parent DOM.
    def parentDOM(self):
        return self.parent

    ## Sets parent DOMElement.
    def setParentDOM(self, dom):
        self.parent = dom

    ## \Returns basic structure element.
    def getDataType(self):
        return self.dep.getType()

    ## \Returns own Depenency
    def getDependency(self):
        return self.dep

    ## \Returns value of currently selected element. Pure virtual, has
    # to be overriden in descendant classes.
    def getValue(self):
        pure_virtual()

    def setValue(self, v):
        pure_virtual()

    ## \Returns statically determinable size in bytes or -1.
    def getByteSize(self):
        return -1

    ## Implementation of Traversable interface.
    def Up(self):
        return self.parent

    ## Implementation of Traversable interface.
    def Down(self, index):
        val = self.getValue()
        xassert(isinstance(val, list))
        res = val[index]
        return res

    def visit(self, visitor):
        visitor.VisitDOMElement(self)

    ## \Returns string filled with some debugging informations.
    def debug_string(self):
        s = self.dep.getName()
        s += " [offset=%s, ownindex=%s" % (str(self.akt_offs), str(self.own_index))
        if isinstance(self, DOMArray):
            s += ", array_size=%s, curr_elem=%s" % (str(self.getSize()),
                                                    str(self.getCurrentElement()))
        elif isinstance(self, DOMAlternative):
            s += ", alternative=%s" % str(self.getCurrentElement())
        s += "]"
        return s

## This class represents empty space or missing element. Used as
# alternative, in case a datum is optional.
class DOMVoid(DOMElement):
    def __init__(self, reader, dep, own_index, parent):
        super(DOMVoid, self).__init__(reader, dep, own_index, parent)

    def Down(self, _):
        impossible()

    def visit(self, visitor):
        visitor.VisitDOMVoid(self)

    def save(self, _):
        pass

    def setValue(self, v):
        if not (isinstance(v, tuple) and len(v) == 0
                or isinstance(v, types.NoneType)):
            raise DOMException("Value is not None or ().")
        return

## This class is a wrapper for basic types Char, Byte, Word and DWord.
class  DOMBasic(DOMElement):
    def __init__(self, reader, dep, own_index, parent):
        super(DOMBasic, self).__init__(reader, dep, own_index, parent)
        self._value = None
        self.type = self.dep.getType().type()
        
    def __str__(self):
        return self.dep.getName() + "(" + str(self._value) +")"        

    def Init(self):
        try:
            if self.dep.init is None:
                DOMElement.Init(self)
                self._value = self.type.load(self.reader, 1)
            else:
                self._value = Attribute(self.dep.init)
                self._value.compute(self)
        except Reader.SafeReaderException, e:
            RaiseMoreInfoAboutProblem(e, self)
                        

    def getValue(self):
        if self.dep.init is None:
            return self._value
        else:
            return self._value.value()

    def setValue(self, v):
        if self.dep.init is not None:
            raise DOMException("Cannot initialize attribute by value.")
        if not isinstance(v, (types.StringType, int, long)):
            raise DOMException("Value is not a string or an int or a long.")
        self._value = v

    def getByteSize(self):
        if self.dep.init is None:
            return self.dep.getSizeOf()
        else:
            return 0

    def Down(self, _):
        impossible()

    def visit(self, visitor):
        visitor.VisitDOMBasic(self)

    def save(self, writer):
        if self.dep.onsave is not None:
                self._value = self.dep.onsave.getDOMValue(self)
        xassert(self._value is not None)
        if not isinstance(self._value, Attribute):
            self.dep.getType().type().save(writer, self._value, 1)
                


class DOMBasicArray(DOMBasic):
    def Init(self):
        try:
            DOMElement.Init(self)
            
            self._value = self.dep.getType().type().load(self.reader,
                                                     self.dep.size.getDOMValue(self))
        except Reader.SafeReaderException, e:
            RaiseMoreInfoAboutProblem(e, self)
        
    def getByteSize(self):
        size = None
        size = (self.dep.getType().type().get_basic_element().getStaticSize()
                    * self.dep.size.getDOMValue(self))
        return size

    def getElementAt(self,idx):
        dom = DOMStruct(None,None,None)
        dom.value = self._value[idx];
        return dom

    def save(self, writer):
        size = self.dep.size.getDOMValue(self)
        self.dep.getType().type().save(writer, self._value, size)

    def setValue(self, v):
        if not isinstance(v, (list, str)):
            raise DOMException("Value has to be a list of values or a string.")
        size = self.dep.size.getDOMValue(self)
        if not len(v) == size:
            raise DOMException("Length of initializer list is not == specified size.")

        #TODO: Add type checking of values in the list.

        self._value = v
        return



class DOMStruct(DOMElement):
    def __init__(self, reader, dep, own_index = None, parent = None):
        super(DOMStruct, self).__init__(reader, dep, own_index, parent)

        self.value = []

        #VO this is a hack, memarrays have to return some traversable dom
        # so they simulate it via empty struct, and dont change the internal
        # state of the array
        if dep == None: return

        for i,agg in enumerate(self.dep.getAggregates()):
            e = self.createDOMElement(agg, i)
            self.value.append(e)
        self.byte_size = None

    def Init(self):
        try:
            DOMElement.Init(self)
            self.setCurrentElement(0)
            for i in self.value:
                i.Init()
        except Reader.SafeReaderException, e:
            RaiseMoreInfoAboutProblem(e, self)                

    def get(self, name):
        for i in xrange(len(self.dep.getType().fields)):
            if self.dep.getType().fields[i].name == name:
                return self.value[i]
        return None

    #returns dom aggregates
    def getValue(self):
        return self.value

    def setValue(self, v):
        if not isinstance(v, tuple) and not isinstance(v, list):
            raise DOMException("Value has to be a tuple or list of values.")

        deps = self.dep.getAggregates()
        if len(v) != len(deps):
            raise DOMException("Not enough initializers.")

        for i in xrange(len(v)):
            part = v[i]
            agg = deps[i]
            if isinstance(part, DOMElement):
                agg_class = agg.getType().__class__
                if isinstance(part.dep.getType(), agg_class):
                    self.value[i] = part
                    part.setOwnIndex(i)
                else:
                    raise DOMException(
                        "Type of passed value does not match declaration.")
            else:
                dom = self.createDOMElement(deps[i], i)
                dom.setValue(part)
                self.value[i] = dom

        return

    #TODO deescribe propely this interface method
    def getByteSize(self):
        if self.byte_size is not None:
            return self.byte_size
        size = 0
        for i in xrange(len(self.value)):
            sz = self.value[i].getByteSize()
            if sz != -1:
                size += sz
            else:
                size = -1
                break
        self.byte_size = size
        return size

    def Down(self, index):
        xassert(index < len(self.value))
        return self.value[index]

    def visit(self, visitor):
        visitor.VisitDOMStruct(self)

    def save(self, writer):
        DOMElement.save(self,writer)
        for i in self.value:
            i.save(writer)
        return


class DOMArray(DOMElement):
    def __init__(self, reader, dep, own_index, parent):
        super(DOMArray, self).__init__(reader, dep, own_index,
                                       parent)
        self.value = []
        ## Size of array in elements.
        self.array_size = None
        ## Aggregate's size. Filled byt descendandt class.
        self.agg_size = None
        self.current_elem = -1

        self.memory_elements = {}
        for i,agg in enumerate(self.dep.getAggregates()):
            self.value.append(self.createDOMElement(agg, 0))
            if agg.in_memory:
                self.memory_elements[agg.getType()] = []

        self.current_elem = -1

    def _load_in_memory(self, in_mem_array, agg, dep, curr_agg):
        try:
            for i in xrange(self.array_size):
                #when loading the memory array the tree must be in consistent state
                for k,agg in enumerate(self.dep.getAggregates()):
                    if k == curr_agg:
                        break
                    if agg.in_memory:
                        self.value[k] = self.memory_elements[agg.getType()][i]
                self.current_elem = i;
                if len(in_mem_array) <=i:
                    in_mem_array.append(self.createDOMElement(agg, i))
                    in_mem_array[i].setOwnIndex(i)
                in_mem_array[i].Init()
        except VE.StopParseException, inst:
            self.array_size = self.current_elem
            

    def Init(self):
        try:
            DOMElement.Init(self)
            xassert(self.dep.size is not None)
            self.array_size = self.dep.size.getDOMValue(self)
            xassert(self.array_size >= 0)
    
            if self.array_size == 0:
                return
    
            self.current_elem =0;
    
            for i,agg in enumerate(self.dep.getAggregates()):
                if agg.in_memory:
                    self._load_in_memory(self.memory_elements[agg.getType()],agg.getType(),agg, i)
                    self.value[i] = self.memory_elements[agg.getType()][0]
                else:
                    self.value[i].Init()
                    
            if self.array_size == 0:
                return                
            self.loadIndex(0);
        except Reader.SafeReaderException, e:
            RaiseMoreInfoAboutProblem(e, self)            

    # nestavove API pro ziskani prvku na urcitem indexu
    def getElementAt(self, idx):
        xassert(idx<self.array_size)

        if self.current_elem == idx:
            return self
        else:
            dom = DOMStruct(None,None,None)
            dom.value = []

            for i,agg in enumerate(self.dep.getAggregates()):
                if agg.in_memory:
                    if  len(self.memory_elements[agg.getType()]) > idx:
                        dom.value.append( self.memory_elements[agg.getType()][idx] )
                    else:
                        dom.value.append(DOMStruct(None,None,None))
                else:
                    dom.value.append(None)

            return dom

    def loadIndex(self, idx):
        try:
            xassert(idx < self.array_size, "Index not in bounds")
            self.current_elem = idx
    
            for i,agg in enumerate(self.dep.getAggregates()):
                if agg.in_memory:
                    self.value[i] = self.memory_elements[agg.getType()][idx]
                else:
                    self.value[i].setOwnIndex(idx)
                    self.value[i].Init()
                setattr(self, agg.getName(), self.value[i] )
    
            return self
        except Reader.SafeReaderException, e:
            RaiseMoreInfoAboutProblem(e,self)
            
    def getValue(self):
        xassert(self.array_size != 0,
                "Array size is 0, it is not possible to get a value")
        return self.value

    def setValue(self, v):
        if not isinstance(v, list):
            raise DOMException("Initializer is not a list of values.")

        deps = self.dep.getAggregates()

        for val_i in xrange(len(v)):
            part = v[val_i]
            for i in xrange(len(deps)):
                sub_part = part[i]
                dep = deps[i]
                dep_type = dep.getType()
                new_el = None
                if dep.in_memory:
                    if val_i < len(self.memory_elements[dep_type]):
                        if isinstance(sub_part, DOMElement):
                            new_el = sub_part
                            self.memory_elements[dep_type][val_i] = sub_part
                        else:
                            new_el = self.memory_elements[dep_type][val_i]
                            new_el.setValue(sub_part)
                    else:
                        xassert(val_i == len(self.memory_elements[dep_type]))
                        if isinstance(sub_part, DOMElement):
                            new_el = sub_part
                        else:
                            new_el = self.createDOMElement(dep, val_i)
                            new_el.setValue(sub_part)
                        self.memory_elements[dep_type].append(new_el)
                else:
                    if isinstance(sub_part, DOMElement):
                        new_el = sub_part
                    else:
                        new_el = self.createDOMElement(dep, val_i)
                        new_el.setValue(sub_part)
                self.value[i] = new_el

        # Trim all excess elements, if there are any.
        for arr in self.memory_elements.itervalues():
            del arr[len(v):]

        self.array_size = len(v)
        self.setCurrentElement(len(v) - 1)

    def append(self, v):
        if self.array_size is None:
            self.array_size = 0
        self.array_size += 1
        val_i = self.array_size

        # Trim all excess elements, if there are any.
        for arr in self.memory_elements.itervalues():
            del arr[val_i:]

        deps = self.dep.getAggregates()
        part = v
        for i in xrange(len(deps)):
            sub_part = part[i]
            dep = deps[i]
            dep_type = dep.getType()
            new_el = None
            if dep.in_memory:
                if isinstance(sub_part, DOMElement):
                    new_el = sub_part
                else:
                    new_el = self.createDOMElement(dep, val_i)
                    new_el.setValue(sub_part)
                self.memory_elements[dep_type].append(new_el)
            else:
                if isinstance(sub_part, DOMElement):
                    new_el = sub_part
                else:
                    new_el = self.createDOMElement(dep, val_i)
                    new_el.setValue(sub_part)
            self.value[i] = new_el

        self.setCurrentElement(self.array_size - 1)

    def getSize(self):
        return self.array_size

    def visit(self, visitor):
        visitor.VisitDOMArray(self)

    ## simple iterator implementation
    #  iterator changes the internal state of the DOM !
    def __iter__(self):
        self.current_elem = -1
        return self

    def next(self):
        self.current_elem = self.current_elem + 1
        if self.current_elem >= self.array_size:
            raise StopIteration()
        self.loadIndex(self.current_elem)
        return tuple(self.value)

    def save(self, writer):
        DOMElement.save(self,writer)
        for agg_i in xrange(len(self.dep.getAggregates())):
            for i in xrange(self.getSize()):
                self.loadIndex(i)
                self.value[agg_i].save(writer)
        return


## This class represents the possibility of alternative representations of data
# that's interpretation depends on some selector function.
class DOMAlternative(DOMElement):
    def __init__(self, reader, dep, own_index, parent):
        super(DOMAlternative, self).__init__(reader, dep, own_index,
                                             parent)
        self.alternatives = []

        for i, agg in enumerate (self.dep.getAggregates()):
            e = self.createDOMElement( agg, 0)
            self.alternatives.append(e)
        self.value = self.alternatives[0]

    def Init(self):
        try:
            DOMElement.Init(self)
            current_alternative = self.dep.selector.getDOMValue(self)
            
            if current_alternative < 0 or current_alternative >= len(self.alternatives):
                raise DOMException("The expected alternative index "+str(current_alternative)+" does not exist in \n"+self.getDependency().getFullPath())
            
            self.setCurrentElement(current_alternative)
            self.reader.seek(self.akt_offs)
    
            self.value = self.alternatives[current_alternative]
            self.value.Init()
        except Reader.SafeReaderException, e:
            RaiseMoreInfoAboutProblem(e,self)


    def getValue(self):
        return self.value

    def setValue(self, v):
        old_alt = self.getCurrentElement()

        try:
            current_alternative = self.dep.selector.getDOMValue(self)
            self.setCurrentElement(current_alternative)
            self.value = self.alternatives[current_alternative]
            self.value.setValue(v)
        except:
            self.setCurrentElement(old_alt)
            self.value = self.alternatives[old_alt]
            raise

        return

    def Down(self, index):
        if index != self.current_elem:
            raise DOMException(
                "Selected alternative is not possible, check selector in \n %s element" % self.getDependency().getFullPath()) #(self.dep.getName()) % self.getPath() )
        return self.value


    def get(self, name):
        for i, agg in enumerate( self.dep.getAggregates() ):
            if agg.getName() == name:
                if self.current_elem == i:
                    return self.getValue()
        impossible()

    def visit(self, visitor):
        visitor.VisitDOMAlternative(self)

    def save(self, writer):
        DOMElement.save(self,writer)        
        self.value.save(writer)

