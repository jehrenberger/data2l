import copy
from ValueExtractors import ValueGetter
from Assertion import Assertion
from exprs.ExprVE import ExprVE
from exprs.Eval import context
from Traversal import Traversable
from Utility import xassert, xassert_type, validate_ident, valid_identifier_regex
import DataTypes as DT
import BasicTypes as BT
from exprs.Eval import ENameNotFound

__all__ = """\
Dependency
CompiledDependency
DependencyDefinitionException
""".split()

class DependencyDefinitionException(Exception):
    def __init__(self, msg):        
        self.msg = msg
    
    def __str__(self):
        return self.msg

## This class gathers are "dependencies" of particular use of a basic
# structure element.
# \todo It should probably be named Annotation, or more probably Instance ?.
class Dependency(Traversable):
    def __init__(self, offset = None, size = None, selector = None, 
                 onsave = None, init = None, in_memory = None, assertions = None, 
                  customProperties = {}):

        ## List of assertions.
        self.assertions = []
        if assertions is not None:
            self.addAssertions(assertions)
                
        self.customProperties = {}
        for k,v in customProperties.items():
            self.addProperty(k, v)

        ## In case of array, this property indicates that it should be
        # kept whole in memory.
        if in_memory is not None:
            xassert(isinstance(in_memory, bool))
            self.in_memory = in_memory
        else:
            self.in_memory = False

        ## Offset of the dependent structure.
        self.offset = self.size = self.selector = self.onsave = self.init = None
        self.setGetter("offset", offset)
        self.setGetter("size", size)
        self.setGetter("selector", selector)
        self.setGetter("onsave", onsave)
        self.setGetter("init", init)

        ## Dependency of this DataElement.
        self.element = None


    ## Sets self.element
    def setElement(self, element):
        self.element = element

    def getType(self):
        return self.element
    
    ## \Returns self.element
    # \deprecated 
    def getElement(self):
        return self.element

    def setGetter(self, name, getter):
        if getter is None:
            return
        if isinstance(getter,str):
            setattr(self, name, ExprVE(getter))
        else:
            xassert_type(getter, ValueGetter)
            setattr(self, name, getter)

    def addAssertions(self, assertions):
        if assertions is not None:
            xassert(isinstance(assertions, list))
            for a in assertions:
                xassert(isinstance(a, Assertion))
                self.assertions.append(a)

    def getAggregates(self):
        return self.getType().getDeps()

    def isComposite(self):
        xassert((not (isinstance(self.element, DT.Basic) or isinstance(self.element, DT.Void)))
                ==    isinstance(self.element, DT.Composite))
        return isinstance(self.element, DT.Composite)

    def isArray(self):
        if isinstance(self.element, DT.Array):
            return True
        if isinstance(self.element, DT.Basic):
            if isinstance(self.element.basic_type, BT.BasicArrayType):
                return True
        return False;

    def getName(self):
        return self.element.name
    
    
    def getProperties(self):
        return self.customProperties
    
    ## returns custom property according to a key
    def getProperty(self,key):
        if  self.hasProperty(key):
            return self.customProperties[key]
        else:
            return None
    
     ## 
    def hasProperty(self,key):
        return self.customProperties.has_key(key)
   
    
    ## adds dependencies custom property
    def addProperty(self,key,value):
        if not validate_ident(key):
            raise DependencyDefinitionException(
                "Incorrect property identifier name: %s\nIdentifier has to match "
                "regex %s" % (key, valid_identifier_regex))
        self.customProperties[key] = value

    def updateKeyProperty(self,old_key, new_key):
        value  = self.customProperties[old_key]
        del self.customProperties[old_key]
        self.customProperties[new_key] = value
    
    def updateValueProperty(self,key, new_value):
        self.customProperties[key] = new_value
    
    ## deletes dependencies custom property
    def delProperty(self,key):
        del self.customProperties[key]



class CompiledDependency(Dependency):
    def __init__(self, dep):
        Dependency.__init__(self, copy.copy(dep.offset),
                                  copy.copy(dep.size),
                                  copy.copy(dep.selector),
                                  copy.copy(dep.onsave),
                                  copy.copy(dep.init),
                                  dep.in_memory,
                                  copy.copy(dep.assertions),
                                  copy.copy(dep.customProperties) )
        self.element = dep.element
        self.parent = None
        self._currently_walked_dep = None

        self.aggregates = []
        for i in self.element.getDeps():
            d = CompiledDependency(i)
            d.setParent(self)
            self.aggregates.append(d)

    def setParent(self,new_parent):
        self.parent = new_parent

    def getParent(self):
        return self.parent

    def getAggregates(self):
        return self.aggregates

    #returns static size of the structure or array or element
    def getSizeOf(self):
        size = 0
        if isinstance(self.element, DT.Array) or isinstance(self.element, DT.Alternative):
            return -1
        
        if isinstance(self.element, DT.Basic) and not isinstance(self.element, DT.Attrib):
            try:
                if self.size is None:
                    size = self.element.getElementSize()
                    return size
                simple_array_size = self.size.getDOMValue(None)
            except:
                return -1
            size = self.element.getElementSize()* simple_array_size    
            return size
            
        for agg in self.getAggregates():
            if agg.getSizeOf() < 0:
                return -1
            size = size + agg.getSizeOf()
        return size
        

    
    ## Walks dependencies recursively and compiles all ExprVEs into ValueGetters.
    def compileExprs(self):
        
        # There is not necessary to call the walk function, 
        # instead you can call the self._walk_value_getters itself
        # The walk function is a residue from previous version of 
        # the compiler.     
        
        for d in self.getAggregates():
            self._currently_walked_dep = d
            try:
                actStr = "Offset getter"
                if d.offset is not None:
                    d.offset.walk(self._walk_value_getters)
                actStr = "Size getter"    
                if d.size is not None:
                    d.size.walk(self._walk_value_getters)
                actStr = "Selector getter"    
                if d.selector is not None:
                    d.selector.walk(self._walk_value_getters)
                actStr = "On Save getter"    
                if d.onsave is not None:
                    d.onsave.walk(self._walk_value_getters)
                actStr = "Init getter"    
                if d.init is not None:
                    d.init.walk(self._walk_value_getters)
                actStr = "Validation section"    
                for a in d.assertions:
                    getter = a.getCode()
                    getter.walk(self._walk_value_getters)
            except ENameNotFound, e:     
                raise ENameNotFound(e.getName() + "\ncontained in the code of the " + actStr , e.getPath())
            
            d.compileExprs()
            self._currently_walked_dep = None

    ## Internal callback for ValueGetter.walk() method.
    def _walk_value_getters(self, getter, sub_getters):
        if isinstance(getter, ExprVE):
            context.compile(getter, self._currently_walked_dep)
        for g in sub_getters:
            g.walk(self._walk_value_getters)

    def Up(self):
        return self.parent

    def Down(self, index):
        deps = self.getAggregates()
        xassert(index < len(deps))
        return deps[index]
    
    def getFullPath(self, separator = "."):
        path = self.getElement().getName()
        dep = self.Up()        
        while(dep != None):
            path = dep.getElement().getName() + separator + path
            dep = dep.Up() 
        return path

