import __builtin__
import gtk
import copy
import re
import imp
import BasicTypes
import DataTypes
import Dependency
import exprs.ExprVE as ExprVE
from gtkGUI import TextViewDialog, Command, CheckBoxDialog, clipboard, Config, EnumsDialog
from gtkGUI.Utils import refresh_tree_row, validate_ident_with_err_dialog
import Assertion as A
import file_defs.prelude
import Cheetah.Template
from generators.NewFormDef import NewFormDef as CompositeGen
from DefWalk import IDefWalkEvents, DefinitionWalker
from Utility import xassert, xassert_type, print_trace, impossible
import gobject
import pango
from language.LanguageRW import *


__all__ = """\
NameRenderer
TypeRenderer
NAME_COL
TYPE_COL
StructTree
""".split()


def NameRenderer(treeviewcolumn, cell_renderer, model, iter, user_data = None):
    dep = model.get_value(iter, 0)
    if dep is None:
        return
    cell_renderer.set_property('text', dep.getName())
    if isinstance(dep.getType(), DataTypes.Attrib):
        cell_renderer.set_property('style', pango.STYLE_ITALIC)
    else:
        cell_renderer.set_property('style', pango.STYLE_NORMAL)
    
    if model.iter_children(iter) is None:
        for i in dep.getType().getDeps():
            #print i.getElement().getName()
            model.append(iter, [i])
    return


def TypeRenderer(treeviewcolumn, cell_renderer, model, iter, user_data = None):
    dep = model.get_value(iter, 0)
    type_str = "unknown"
    if isinstance(dep.getType(), DataTypes.Void) : type_str = 'Void'
    elif isinstance(dep.getType(), DataTypes.Struct) : type_str = 'Struct'
    elif isinstance(dep.getType(), DataTypes.Array) : type_str = 'Array'
    elif isinstance(dep.getType(), DataTypes.Alternative) : type_str = 'Alternative'
    elif isinstance(dep.getType(), DataTypes.Basic) :
        type_str = str(dep.getType().basic_type)    
        if (isinstance(dep.getType().type(), BasicTypes.BasicNumType) and
            dep.getType().type().getEnum() is not None):
            type_str = dep.getType().type().getEnum().getName() + ":" + type_str

            
    if isinstance(dep.getType(), DataTypes.Attrib):
        cell_renderer.set_property('style', pango.STYLE_ITALIC)
        type_str = "Attr:" + type_str
    else:
        cell_renderer.set_property('style', pango.STYLE_NORMAL)
    
    cell_renderer.set_property('text', type_str)
    return


NAME_COL = 0
TYPE_COL = 1

_PROPS_PROP_COL = 0
_PROPS_VAL_COL = 1


class StructTree(object):
    DND_TARGETS = [('text/plain', 0, 0),
                   ('DT_TREE_MODEL_ROW', gtk.TARGET_SAME_WIDGET, 1),
                   ('STRING', 0, 2),
                   ('TEXT', 0, 3)]
    DND_SOURCES = DND_TARGETS
                   

    def __init__(self, main, xml, definition):
        ## The Main object.
        self.main = main
        ## TreeView widget.
        self.tree = xml.get_widget('structuretree')
        ## Data of the TreeView.
        self.treestore = gtk.TreeStore(object)
        ## Popup menu.
        self.popup_menu = None

        self.nameCell = gtk.CellRendererText()
        self.nameCell.connect('edited', self.name_column_edit_handler,
                         self.treestore)
        self.nameCell.connect('editing-started', self.name_column_start_edit_handler)

        self.tree.insert_column_with_data_func(
            NAME_COL, 'Name', self.nameCell, NameRenderer)
        self.tree.insert_column_with_data_func(
            TYPE_COL, 'Type', gtk.CellRendererText(), TypeRenderer)

        self.tree.get_column(NAME_COL).set_resizable(True)
        self.tree.get_column(TYPE_COL).set_resizable(True)

        self.tree.set_model(self.treestore)
        self.tree.connect('button_press_event', self.show_context_menu)
        self.tree.connect('cursor-changed', self.tree_cursor_changed)
        if (gtk.pygtk_version >= (2, 10, 0)
            and gtk.gtk_version >= (2, 10, 0)):
            self.tree.set_enable_tree_lines(True)
        self.tree_selection = self.tree.get_selection()
        self.tree_selection.connect('changed', self.selection_changed)
        self.tree_selection.set_mode(gtk.SELECTION_BROWSE)
        self.deps_tab_viewport = xml.get_widget('deps_tab_viewport')
        self.deps_tab_viewport.set_sensitive(False)
        self.setDefinition(definition)

        self.tree.enable_model_drag_dest(StructTree.DND_TARGETS,
                                         gtk.gdk.ACTION_DEFAULT |
                                         gtk.gdk.ACTION_COPY |
                                         gtk.gdk.ACTION_MOVE)
        self.tree.enable_model_drag_source(
            gtk.gdk.BUTTON1_MASK, StructTree.DND_SOURCES,
            gtk.gdk.ACTION_DEFAULT | gtk.gdk.ACTION_MOVE
            | gtk.gdk.ACTION_COPY | gtk.gdk.ACTION_ASK)
        self.tree.connect("drag_data_get",
                          self.drag_data_get_data)
        self.tree.connect("drag_data_received",
                          self.drag_data_received_data)        
        self.tree.connect_after("drag_begin", 
                                self.drag_begin_handler)
        self.tree.connect("drag_end",
                          self.drag_end_handler)
        
        self._src_path = None
     
    def setDefinition(self, definition):
        self.treestore.clear()
        if definition is not None:
            self.treestore.append(None, [definition.dep_root])
        self.definition = definition

        self.main.depsView.setDefinition(definition)
    
    def drag_begin_handler(self, tree, context):                
        BORDER_HORIZONTAL = 20
        BORDER_VERTICAL = 4
        MIN_WIDTH = 150

        # Get text from tree
        treeselection = tree.get_selection()
        model, iter = treeselection.get_selected()
        dep = model[iter][NAME_COL]
        
        self.dragText = str(dep.getType())
        
        layout = tree.create_pango_layout(self.dragText)
        width, height = layout.get_pixel_size()
        pixmap_width = max(width, MIN_WIDTH) + BORDER_HORIZONTAL * 2
        pixmap_height = height + BORDER_VERTICAL * 2

        cm=gtk.gdk.colormap_get_system()
        pixmap = gtk.gdk.Pixmap(None,
                                pixmap_width, pixmap_height, cm.get_visual().depth)

        # White background
        cr = pixmap.cairo_create()
        cr.set_source_rgb(1., 1., 1.)
        cr.paint()
        
        # Black outline
        cr.set_source_rgb(0., 0., 0.,)
        cr.rectangle(0.5, 0.5, pixmap_width - 1, pixmap_height - 1)
        cr.set_line_width(1)
        cr.stroke()

        # Add the text
        cr.move_to(BORDER_HORIZONTAL, BORDER_VERTICAL)
        cr.show_layout(layout)
        
        mask = gtk.gdk.Pixmap(None,
                              pixmap_width, pixmap_height, 1) # depth 1 == bitmap
        cr = mask.cairo_create()
        cr.set_source_rgb(1., 1., 1.)
        cr.paint()

        context.set_icon_pixmap(pixmap.get_colormap(), pixmap, mask, 0, 0)
        self.main.showStatusText("Drag&Drop: " + self.dragText)        
            
    def drag_end_handler(self, tree, context):
        # clear status bar
        self.main.showStatusText()

    def getD2LTextFromDep(self, dep):
        text = None
        if isinstance(dep.getElement(), DataTypes.Composite):
            text = LanguageRW.writeLanguage(dep)
        else:
            text = LanguageRW.makeSimpleText(dep)
        return text
    
    def getDependencyFromD2LText(self, text):        
        entity_dep = None            
        try:
            entity_dep, _ = LanguageRW.readLanguage(text)
        except Exception, e:
            entity_dep = None
            print str(e)
        if entity_dep == None:
            try:
                entity_dep =  LanguageRW.parseSimpleText(text)                
            except Exception, e:
                entity_dep = None
                print str(e)
        return entity_dep

    def drag_data_get_data(self, treeview, context, selection, target_id,
                           time):
        #print_trace()
        treeselection = treeview.get_selection()
        model, iter = treeselection.get_selected()
        dep = model[iter][NAME_COL]
        self._src_path = model.get_path(iter)
        # Serialize the selected declaration.        
        text = self.getD2LTextFromDep(dep)
            
        # Store the serialized text.
        selection.set(selection.target, 8, text)
        return


    def drag_data_received_data(self, treeview, context, x, y, selection,
                                info, etime):
        data = selection.data
        model = treeview.get_model()
        drop_info = treeview.get_dest_row_at_pos(x, y)
        if drop_info:            
            entity_dep = self.getDependencyFromD2LText(data)
            if entity_dep == None:
                return
            entity = entity_dep.getElement()
                        
            dest_path, position = drop_info
            dest_index = None
            src_full_path = self._src_path
            dest_full_path = dest_path = list(dest_path)

            if (position == gtk.TREE_VIEW_DROP_BEFORE
                or position == gtk.TREE_VIEW_DROP_INTO_OR_BEFORE):
                dest_index = dest_path[-1]
                dest_path = tuple(dest_path[:-1])
            else:
                dest_index = dest_path[-1] + 1
                dest_path = tuple(dest_path[:-1])
            if len(dest_path) == 0:
                return
            iter = model.get_iter(dest_path)
            dest_dep = model[iter][NAME_COL]

            # moving is enabled only inside one treeview widget, other do copy only
            if context.action == gtk.gdk.ACTION_MOVE and context.get_source_widget() == self.tree:
                # TODO: Kopirovat/presouvat strukturu samu do sebe, je to prakticke?
                # Osetrit.
                src_index = self._src_path[-1]
                self._src_path = self._src_path[:-1]
                if len(self._src_path) == 0:
                    return
                src_iter = model.get_iter(self._src_path)
                src_dep = model[src_iter][NAME_COL]

                dest_info = (dest_path, iter, dest_dep, dest_index, model)
                if self._src_path == dest_path:
                    if src_index > dest_index:
                        src_index += 1
                src_info = (self._src_path, src_iter, src_dep, src_index,
                            model)
                command_insert = Command.InsertElement(dest_dep, dest_index,
                                                       entity)
                command_change_dep = Command.ChangeElement(
                    dest_dep, dest_index, entity, entity_dep)
                command_delete = Command.DeleteElement(src_dep, src_index)
                command = Command.GUICommand()
                command.add_sub_command(
                    command_insert,
                    lambda: self.refresh_after_insert(dest_info),
                    lambda: self.refresh_after_delete(dest_info))
                command.add_sub_command(
                    command_change_dep,
                    lambda: self.refresh_after_dep_exchange(
                        tuple(list(dest_path) + [dest_index]), entity_dep))
                command.add_sub_command(
                    command_delete,
                    lambda: self.refresh_after_delete(src_info),
                    lambda: self.refresh_after_insert(src_info))

                self.main.do_command(command)

            elif (context.action == gtk.gdk.ACTION_COPY or # if moving requested from other widget, do copy only TODO: solve better
                 (context.action == gtk.gdk.ACTION_MOVE and context.get_source_widget() != self.tree)):
                info = (dest_path, iter, dest_dep, dest_index, model)
                command_insert = Command.InsertElement(
                    dest_dep, dest_index, entity)
                command_change_dep = Command.ChangeElement(
                    dest_dep, dest_index, entity, entity_dep)
                command = Command.GUICommand()
                command.add_sub_command(
                    command_insert,
                    lambda: self.refresh_after_insert(info),
                    lambda: self.refresh_after_delete(info))
                command.add_sub_command(
                    command_change_dep,
                    lambda: self.refresh_after_dep_exchange(
                        tuple(list(dest_path) + [dest_index]), entity_dep))
                self.main.do_command(command)

            context.finish(True, False, etime)
        else:
            context.finish(False, False, etime)

        return    

    def find_influenced_getters(self,dep, name, visited, list):
        if visited.has_key(dep):
            return
        visited[dep] = 1

        p = re.compile( r'(\b|\W)%s(\b|\W)' % (name) )
        for i in ('size', 'offset', 'init', 'selector'):
            getter = getattr(dep, i)
            if isinstance(getter,ExprVE.ExprVE):
                if p.search( getter.getText() ):
                     list.append((dep,getter,i))

        for v in dep.assertions:
            validator = v.getCode()
            if isinstance(validator ,ExprVE.ExprVE):
                if p.search( validator.getText() ):
                     list.append((dep,validator,"validator"))

        for agg in dep.getType().getDeps():
            self.find_influenced_getters(agg,name,visited,list)

    def name_column_start_edit_handler(self, cell, editable, text):
        self.setEditable(False)

    def name_column_edit_handler(self, cell, path, new_text, model):        
        if not validate_ident_with_err_dialog(new_text):
            return
                
        iter = model.get_iter(path)
        dep = model[iter][NAME_COL]
        
        if new_text == dep.getName():
            return

        visited = {}
        influenced_list = []
        self.find_influenced_getters(self.definition.dep_root, dep.getName(), visited,

                                     influenced_list)

        command = Command.GUICommand(lambda: self.main.refresh_gui())

        if len(influenced_list) > 0:
            check_list = []
            for i in influenced_list:
                check_list.append([False, i[0].getName(), i[2], i[1].getText()])

            d = CheckBoxDialog.CheckBoxDialog(
                'Select %s references in getters to change' % dep.getName(),
                check_list)
            if d.run():
                for i,item in enumerate(check_list):
                    if item[0]: #if checked do the change in getter
                        p = re.compile(r'(\b|\W)%s(\b|\W)' % (dep.getName()))
                        new_getter_text = p.sub("\\1"+new_text+"\\2",
                                                influenced_list[i][1].getText())
                        if influenced_list[i][2] == "validator":
                            index = -1
                            for j,v in enumerate(influenced_list[i][0].assertions):
                                if v.getCode() == influenced_list[i][1]:
                                    index = j
                            xassert(index >= 0)
                            c = Command.EditAssertion(influenced_list[i][0],
                                            index,
                                            A.Assertion(new_getter_text,
                                                influenced_list[i][0].assertions[index].getName()))
                        else:
                            c = Command.EditGetter(influenced_list[i][0],
                                               influenced_list[i][2] ,
                                               ExprVE.ExprVE(new_getter_text))
                        command.add_sub_command(c)
            else:
                d.hide()
                return  #no rename if cancel is pressed

            d.hide()

        rename_command = Command.RenameElement(dep.getElement(), new_text)
        command.add_sub_command(rename_command)
        self.main.do_command(command)

    props_type_items = [('Byte', BasicTypes.Byte),
                        ('Char', BasicTypes.Char),
                        ('Word', BasicTypes.Word),
                        ('DWord', BasicTypes.DWord),
                        ('Float', BasicTypes.Float),
                        ('VarInteger', BasicTypes.VarInteger),
                        ('String', BasicTypes.String),
                        ('StaticSizeArray', BasicTypes.StaticSizeArray)]

    props_elem_type_items = [('Byte', BasicTypes.Byte),
                             ('Char', BasicTypes.Char),
                             ('Word', BasicTypes.Word),
                             ('DWord', BasicTypes.DWord),
                             ('Float', BasicTypes.Float)]

    basic_type_items = [('Byte (8b)', BasicTypes.Byte),
                        ('Char (8b)', BasicTypes.Char),
                        ('Word (16b)', BasicTypes.Word),
                        ('DWord (32b)', BasicTypes.DWord),
                        ('Float (32b)', BasicTypes.Float),
                        ('VarInteger', BasicTypes.VarInteger),
                        ('String', BasicTypes.String)]

    attrib_type_items = [('Byte (8b)', BasicTypes.Byte),
                         ('Char (8b)', BasicTypes.Char),
                         ('Word (16b)', BasicTypes.Word),
                         ('DWord (32b)', BasicTypes.DWord),
                         ('Float (32b)', BasicTypes.Float),
                         ('VarInteger', BasicTypes.VarInteger)]

    def props_text_data_func(self, column, cell, model, iter, user_data = None):
        #print_trace()
        if iter is None:
            cell.set_property('text', "")

        prop = model[iter][_PROPS_PROP_COL]
        if prop in ["type", "element"]:
            cell.set_property('visible', False)
        else:
            cell.set_property('visible', True)


    def tree_cursor_changed(self, widget, data = None):
        #print_trace()
        path, _ = self.tree.get_cursor()
        if path is None:
            return

        model, iter = self.tree.get_selection().get_selected()
        if iter is None:
            return

        dep = model[iter][NAME_COL]
        self.main.docsView.scrollOnTreeViewSelect(dep.getName())
        return

    def selection_changed(self, selection):
        #print_trace()
        _, iter = selection.get_selected()        
        if iter is None:
            self.deps_tab_viewport.set_sensitive(False)
        else:
            self.deps_tab_viewport.set_sensitive(True)
        return
    
    def setEditable(self,editable):        
        self.nameCell.set_property('editable', editable)
    
    def editSelected(self):
        path = self.tree.get_cursor()
        if path is not None:
            self.edit_element_name_on_path(path[0])

    def edit_element_name_on_path(self, path):
        if len(path) != 0:
            self.setEditable(True)
            self.tree.set_cursor(path,self.tree.get_column(NAME_COL),start_editing=True)

    def show_context_menu(self, widget, event):
        if event.button == 1 and  event.type == gtk.gdk._2BUTTON_PRESS:
            xassert(event.window == widget.get_bin_window())
            path = widget.get_path_at_pos( int(event.x), int(event.y) )            
            # it's needed to check if double-click was on currently selected element
            # due to problems when double-click was done on +/- in tree view
            cursor = self.tree.get_cursor()                  
            if path is not None and path[0] == cursor[0]:
                gobject.idle_add(self.editSelected)
                     
        if event.button == 3:            
            xassert(event.window == widget.get_bin_window())
            p = widget.get_path_at_pos( int(event.x), int(event.y) )
            if p is not None:
                widget.set_cursor(p[0])
                self.build_tree_popup_menu(event)

    def build_tree_popup_menu(self, event):
        path, _ = self.tree.get_cursor()
        xassert(path is not None)
        model, iter = self.tree.get_selection().get_selected()
        if iter is None:
            return

        dep = model[iter][NAME_COL]
        xassert_type(dep, Dependency.Dependency)

        menu = gtk.Menu()

        root_selected = model.iter_parent(iter) is None

        if isinstance(dep.getElement(), DataTypes.Composite):
            mi = gtk.ImageMenuItem(stock_id = "gtk-add")
            mi.child.set_label("_Add child")
            submenu = self.build_add_to_composite_menu("child", dep)
            mi.set_submenu(submenu)
            menu.append(mi)

        if not root_selected:
            mi = gtk.ImageMenuItem(stock_id = "gtk-add")
            mi.child.set_label("_Add before")
            submenu = self.build_add_to_composite_menu("before", dep)
            mi.set_submenu(submenu)
            menu.append(mi)

        if not root_selected:
            mi = gtk.ImageMenuItem(stock_id = "gtk-add")
            mi.child.set_label("_Add after")
            submenu = self.build_add_to_composite_menu("after", dep)
            mi.set_submenu(submenu)
            menu.append(mi)

        mi = gtk.MenuItem(label = "_Rename")
        mi.connect("activate", self.rename_element)
        menu.append(mi)

        if not root_selected:
            mi = gtk.ImageMenuItem(stock_id = "gtk-delete")
            mi.connect("activate", self.delete_element_from_tree)
            menu.append(mi)

        if isinstance(dep.getType(), DataTypes.Basic ):
            mi = gtk.MenuItem(label = "_Change Basic Type")            
            submenu = self.build_change_basic_type_menu( dep)
            mi.set_submenu(submenu)
            menu.append(mi)
            
        if (isinstance(dep.getType(), DataTypes.Basic) and 
            isinstance(dep.getType().type(), BasicTypes.BasicNumType)):
            mi = gtk.MenuItem(label = "Assign _Enum")
            submenu = self.build_assign_enum_menu(dep)
            mi.set_submenu(submenu)
            menu.append(mi)
            
        menu.append(gtk.SeparatorMenuItem())
        mi = gtk.MenuItem(label = "Add Validator")
        mi.connect("activate", self.add_validator)
        menu.append(mi)

        menu.append(gtk.SeparatorMenuItem())
        mi = gtk.ImageMenuItem("gtk-copy")
        mi.child.set_label("C_opy")
        mi.connect("activate", self.clipboard_copy)
        menu.append(mi)

        mi = gtk.ImageMenuItem("gtk-paste")
        mi.child.set_label("_Paste")
        mi.connect("activate", self.clipboard_paste)
        if clipboard.valid_clipboard_data():
            mi.set_sensitive(True)
        else:
            mi.set_sensitive(False)        
        menu.append(mi)

        menu.show_all()
        menu.popup(None, None, None, 0, event.time)

    def build_add_to_composite_menu(self, where, akt_dep):
        menu = gtk.Menu()

        mi = gtk.MenuItem('Basic')
        mi.set_submenu(self.build_basic_types_menu(where))
        menu.append(mi)

        mi = gtk.MenuItem('Attribute')
        mi.set_submenu(self.build_attrib_types_menu(where))
        menu.append(mi)

        mi = gtk.MenuItem('Structure')
        mi.connect("activate", self.add_composite_to_tree, DataTypes.Struct,
                   where)
        menu.append(mi)

        mi = gtk.MenuItem('Array')
        mi.connect("activate", self.add_composite_to_tree, DataTypes.Array,
                   where)
        menu.append(mi)

        mi = gtk.MenuItem('Alternative')
        mi.connect("activate", self.add_composite_to_tree, DataTypes.Alternative,
                   where)
        menu.append(mi)

        mi = gtk.MenuItem('Void')
        mi.connect("activate", self.add_composite_to_tree, DataTypes.Void,
                   where)
        menu.append(mi)

        mi = gtk.MenuItem('Existing entity')
        mi.set_submenu( self.build_existing_entities_menu(where, akt_dep) )
        #mi.connect("activate", self.add_existing_child_to_tree, where)
        menu.append(mi)

        return menu
    
    def build_existing_entities_menu(self,where, akt_dep):
        menu = gtk.Menu()        
        
        model, iter = self.tree.get_selection().get_selected()
        if iter is None:
            return
                 
        filter = []
        if where == "child":
            selected_element = model[iter][0].getType()     # same as akt_dep.getType()
        else:
            iter = model.iter_parent(iter)                  # get parent (from current branch in tree)
            selected_element = model[iter][0].getType()
            
        self.get_all_ancestors_and_self(selected_element, filter)        
        
		# TODO: Does it have to be member variable?
        self.entities_map=[]
        self.walk_entities(self.definition.dep_root, filter)

        
        for i in self.entities_map:
            mi = gtk.MenuItem(label = i.getName())
            mi.connect("activate", self.add_existing_child_to_tree, i, where)
            menu.append(mi)
        
        return menu
       
    ## append all founded parents to ancestor_list
    # find out all parents given element - means no only in current branch of tree
    def get_all_ancestors_and_self(self, element, ancestor_list):                
        if len(element.getParents()) == 0:
            return
        
        for p in element.getParents():
            self.get_all_ancestors_and_self(p, ancestor_list)
                            
        ancestor_list.append(element)        
            

    def walk_entities(self, akt_dep, filter):
        for d in akt_dep.getAggregates():
            if d.getType() in self.entities_map:
                continue; # do not walk through the same
            if not d.isComposite():
                continue; # do not include non composite things            
            if d.getType() not in filter:
                self.entities_map.append( d.getType() )
            self.walk_entities( d, filter)
                
    def build_basic_types_menu(self, where):
        menu = gtk.Menu()

        for label, ctor in StructTree.basic_type_items:
            mi = gtk.MenuItem(label = label)
            mi.connect("activate", self.add_basic_to_tree, ctor, where)
            menu.append(mi)
            
        menu.append(gtk.SeparatorMenuItem())    
        for label, ctor in StructTree.props_elem_type_items:
            mi = gtk.MenuItem(label = (label+"[]") )
            mi.connect("activate", self.add_basic_to_tree,  
                                       BasicTypes.StaticSizeArray, 
                                       where, 
                                       ctor)
            menu.append(mi)              

        return menu

    def build_change_basic_type_menu(self,dep):
        xassert( isinstance(dep.getType(), DataTypes.Basic) )
        menu = gtk.Menu()        
        
        for label, ctor in StructTree.basic_type_items:
            mi = gtk.MenuItem(label = label)
            mi.connect("activate", self.change_basic_type_in_tree, dep, ctor, False)
            menu.append(mi)
            
        menu.append(gtk.SeparatorMenuItem())    
        for label, ctor in StructTree.props_elem_type_items:
            mi = gtk.MenuItem(label = (label+"[]") )
            mi.connect("activate", self.change_basic_type_in_tree, dep, ctor, True)
            menu.append(mi)                
            
        return menu
    
    def change_basic_type_in_tree(self, item, dep, ctor, is_simple_array ):
        if is_simple_array:
            command = Command.ChangeBasicType(dep,BasicTypes.StaticSizeArray(ctor()))
        else:
            command = Command.ChangeBasicType(dep, ctor())
            
        self.main.do_command(command,
                             lambda: self.refresh_after_type_change(),
                             lambda: self.refresh_after_type_change())
            
        
    def build_attrib_types_menu(self, where):
        menu = gtk.Menu()

        for label, ctor in StructTree.attrib_type_items:
            mi = gtk.MenuItem(label = label)
            mi.connect("activate", self.add_attrib_to_tree, ctor, where)
            menu.append(mi)

        return menu

    def build_assign_enum_menu(self, akt_dep):
        menu = gtk.Menu()
        
        if akt_dep.getType().type().getEnum() is not None:
            mi = gtk.MenuItem(label = "Unassign")
            mi.connect("activate", self.assign_enum_to_type, akt_dep, None)
            menu.append(mi)        
            menu.append(gtk.SeparatorMenuItem())
        
        for enum in self.definition.userDefinedEnums.getEnumList():
            mi = gtk.CheckMenuItem(label = enum.getName())
            mi.connect("activate", self.assign_enum_to_type, akt_dep, enum)
            menu.append(mi)
            if akt_dep.getType().type().getEnum() == enum:
                mi.set_active(True)
            else:
                mi.set_active(False)
                
        menu.append(gtk.SeparatorMenuItem())
        mi = gtk.MenuItem(label = "_Edit ...")            
        mi.connect("activate", self.edit_enums)
        menu.append(mi)
        
        return menu

    def assign_enum_to_type(self, item, akt_dep, enum):
        xassert(isinstance(akt_dep.getType().type(), BasicTypes.BasicNumType))        
        command = Command.AssignEnum(akt_dep.getType().type(), enum)
        self.main.do_command(command,
                             lambda: self.refresh_after_type_change(),
                             lambda: self.refresh_after_type_change())        

    def edit_enums(self, item = None):
        enums_dialog = EnumsDialog.EditEnumsDialog(self.definition)
        ret = enums_dialog.run()
        enums_dialog.hide()
        if ret == gtk.RESPONSE_OK:
            # do the command group created in dialog
            command = enums_dialog.getCommand()
            # refresh type column should be enough,
            # no other changes could have influence on GUI
            self.main.do_command(command,
                lambda: self.refresh_after_type_change(),
                lambda: self.refresh_after_type_change())

    def _prepare_insert(self, where):
        path, _ = self.tree.get_cursor()
        xassert(path is not None)
        model, iter = self.tree.get_selection().get_selected()
        if iter is None:
            return

        parent_iter = model.iter_parent(iter)

        dep = model[iter][NAME_COL]
        if where == "child":
            index = len(dep.getElement().getAggregates())
        else:
            dep = model[parent_iter][NAME_COL]
            iter = parent_iter
            if where == "before":
                index = path[-1]
            elif where == "after":
                index = path[-1]+1
            else:
                impossible()
            # cut last element of path - needed when after/before selected, used for editing new element
            path = path[:-1]
        return (path, iter, dep, index, model)

    def edit_added_element_name(self, path, index):
        new_element_path = path + (index,)
        self.tree.expand_to_path(new_element_path)
        self.edit_element_name_on_path(new_element_path)

    def add_basic_to_tree(self, item, type_ctor, where, subtype_ctor = None):
        info = path, iter, dep, index, model = self._prepare_insert(where)
        ctor = type_ctor
        new_dep = None
        if type_ctor == BasicTypes.String:
            ctor = lambda: type_ctor()
            new_dep = Dependency.Dependency(size =ExprVE.ExprVE("1"))
        elif type_ctor == BasicTypes.StaticSizeArray:
            ctor = lambda: type_ctor(subtype_ctor())
            new_dep = Dependency.Dependency(size =ExprVE.ExprVE("1"))
        command = Command.InsertElement(dep, index,
                                        DataTypes.Basic("new", ctor()), new_dep)
        self.main.do_command(command,
                             lambda: self.refresh_after_insert(info),
                             lambda: self.refresh_after_delete(info))
        
        self.edit_added_element_name(path, index)

    def add_attrib_to_tree(self, item, ctor, where):
        info = path, iter, dep, index, model = self._prepare_insert(where)
        command = Command.InsertElement(dep, index,
                                        DataTypes.Attrib("new", ctor()))
        self.main.do_command(command,
                             lambda: self.refresh_after_insert(info),
                             lambda: self.refresh_after_delete(info))
        
        self.edit_added_element_name(path, index)

    def add_composite_to_tree(self, item, type_ctor, where):
        info = path, iter, dep, index, model = self._prepare_insert(where)
        ctor = type_ctor
        if ctor == DataTypes.Void:
            ctor = lambda _ : DataTypes.Void()
        command = Command.InsertElement(dep, index, ctor("new"))
        self.main.do_command(command,
                             lambda: self.refresh_after_insert(info),
                             lambda: self.refresh_after_delete(info))
        
        self.edit_added_element_name(path, index)

    def add_validator(self, _):
        path, _ = self.tree.get_cursor()
        if path is None:
            return
        iter = self.treestore.get_iter(path)
        dep = self.treestore[iter][NAME_COL]
        index = len(dep.assertions)
        assertion = A.Assertion(code = "1", name = "")
        command = Command.EditAssertion(dep, index, assertion)
        self.main.do_command(command, self.main.validatorsView.refresh)
        # togle to validator view and edit added assertion
        self.main.notebook.set_current_page(1)
        self.main.validatorsView.add_validator(assertion)

    ## \param path Path to parent of newly inserted element.
    def refresh_after_insert(self, info):
        #print_trace()
        path, iterator, dep, index, model = info
        deps = dep.getElement().getDeps()
        xassert(index < len(deps))
        inserted_dep = deps[index]
        model.insert(iterator, index, [inserted_dep])
        self.main.validatorsView.refresh()
        self.main.docsView.refresh()

    def refresh_after_delete(self, info):
        #print_trace()
        _, iter, _, index, model = info
        child_iter = model.iter_nth_child(iter, index)
        model.remove(child_iter)
        self.main.validatorsView.refresh()
        self.main.docsView.refresh()

    def refresh_after_rename(self):
        refresh_tree_row(self.tree)
        self.main.validatorsView.refresh()
        self.main.docsView.refresh()
        self.tree.queue_draw()

    def refresh_after_type_change(self):
        refresh_tree_row(self.tree)
        self.main.docsView.refresh()
        self.main.depsView.refresh()
        self.tree.queue_draw()

    def refresh_after_dep_exchange(self, path, new_dep):
        iter = self.treestore.get_iter(path)
        self.treestore[iter][NAME_COL] = new_dep
        refresh_tree_row(self.tree, path)
        self.main.docsView.refresh()
        self.main.depsView.refresh()
        self.tree.queue_draw()

    def refresh(self):
        # TODO: This is rather incomplete but at least it fixes update
        # after undo bug.
        path, _ = self.tree.get_cursor()
        if path is not None:
            refresh_tree_row(self.tree)
        self.tree.queue_draw()
        return

    def add_existing_child_to_tree(self, item, elem, where):
        info = path, iter, dep, index, model = self._prepare_insert(where)

        command = Command.InsertElement(dep, index, elem)
        self.main.do_command(command,
                             lambda: self.refresh_after_insert(info),
                             lambda: self.refresh_after_delete(info))


    def rename_element(self, item):
        self.editSelected()

    def delete_element_from_tree(self, item):        
        path, _ = self.tree.get_cursor()
        xassert(path is not None)
        info = path, iter, dep, index, _ = self._prepare_insert("before")
        command = Command.DeleteElement(dep, index)
        self.main.do_command(command,
                             lambda: self.refresh_after_delete(info),
                             lambda: self.refresh_after_insert(info))
        
    ## Select and unwrap (if needed) element on specific path.
    #  Path could be selected from other widget.      
    def select_element(self,treeview_path):
        self.tree.expand_to_path(treeview_path)
        self.tree.get_selection().unselect_all()
        self.tree.get_selection().select_path(treeview_path)
        
        if not self.tree.get_selection().path_is_selected(treeview_path):
        
            for i in range(len(treeview_path)):

                #    Tento rozsahly kod je zde proto, aby dochazelo k vicenasobnemu rozbalovani
                # struktur (stromovych uzlu). Toho by se za normalnich okolnosti dosahlo jednoduchym volanim funkce:
                #
                #    self.main.structTree.tree.expand_to_path(treeview_path)
                #
                #    V tomto pripade ale funkce selze, nebot tree model nemusi obsahovat plnou cestu
                # k hledanemu uzlu. Jednotlive uzly se do stromu pridavaji za behu volanim funkce
                # NameRenderer z modulu StructTree. Volani teto funkce provadi samotny TreeView podle
                # potreby v reakci na uzivatelske akce. Az kdyz je rozkliknut rodicovsky uzel, jsou 
                # renderovany jeho deti a do stromu se pridaji deti techto deti. Stane se tak v prubehu 
                # prekreslovaci faze volane az po skonceni teto (=synchronize_treeview) funkce. To zjevne 
                # znemoznuje v ramci jednoho volani rozkliknout vicero generaci uzlu.
                #    Pro obejiti tohoto omezeni je pouzito dvou nizkourovnovych volani (invalidate -- update)
                # vynucujicich synchronni prekresleni treeview a s tim spojeneho pridavani potomku. 
                # Nezbytnou podminkou funkcnosti tohoto reseni je prekreslovani te casti treeview, na niz se 
                # nachazi prave rozkliknuty prvek. V opacnem pripade dojde k renderovani jinych uzlu. 
                # Je proto nutne pomoci fukce set_cursor nejdrive "zascrollovat" na patricne misto.
                # Pote je samozrejme opet treba prekreslit okno treeview.     
                
                self.tree.expand_row(treeview_path[:i+1], False)
                rect = gtk.gdk.Rectangle(self.tree.get_bin_window().get_geometry()[0], 
                                         self.tree.get_bin_window().get_geometry()[1], 
                                         self.tree.get_bin_window().get_geometry()[2], 
                                         self.tree.get_bin_window().get_geometry()[3])
                self.tree.get_bin_window().invalidate_rect(rect, True)
                self.tree.get_bin_window().process_updates(True)

                self.tree.set_cursor(treeview_path[:i+1], None, False )
                rect = gtk.gdk.Rectangle(self.tree.get_bin_window().get_geometry()[0], 
                                         self.tree.get_bin_window().get_geometry()[1], 
                                         self.tree.get_bin_window().get_geometry()[2], 
                                         self.tree.get_bin_window().get_geometry()[3])
                self.tree.get_bin_window().invalidate_rect(rect, True)
                self.tree.get_bin_window().process_updates(True)

                self.tree.expand_row(treeview_path[:i+1], False)
                rect = gtk.gdk.Rectangle(self.tree.get_bin_window().get_geometry()[0], 
                                         self.tree.get_bin_window().get_geometry()[1], 
                                         self.tree.get_bin_window().get_geometry()[2], 
                                         self.tree.get_bin_window().get_geometry()[3])
                self.tree.get_bin_window().invalidate_rect(rect, True)
                self.tree.get_bin_window().process_updates(True)

            self.tree.get_selection().unselect_all()
            self.tree.get_selection().select_path(treeview_path)
        
        self.tree.set_cursor(treeview_path, None, False )
        

    def clipboard_copy(self, _):
        treeselection = self.tree.get_selection()
        model, iter = treeselection.get_selected()
        dep = model[iter][NAME_COL]
        
        # serialize selected definition
        text = self.getD2LTextFromDep(dep)        
        clipboard.put_into_clipboard(text)        

    def clipboard_paste(self, _):
        try:
            data = clipboard.get_from_clipboard()
            # read clipboard data as D2L definition
            entity_dep = self.getDependencyFromD2LText(data)
            if entity_dep == None:
                raise Exception("Data in clipboard is not valid D2L definition.")
            entity = entity_dep.getElement()
            
            treeselection = self.tree.get_selection()
            model, iter = treeselection.get_selected()
            dest_dep = model[iter][NAME_COL]
            dest_path = model.get_path(iter)
            dest_index = None
            dest_path = list(dest_path)
            if isinstance(dest_dep.getElement(), DataTypes.Composite):
                # Insert as last element of given Composite.
                dest_path = tuple(dest_path)
                it = model.get_iter(dest_path)
                dest_index = len(model[it][NAME_COL].getElement().getDeps())
            else:
                # Insert at position of given Basic element.
                dest_index = dest_path[-1]
                dest_path = tuple(dest_path[:-1])
    
            xassert(len(dest_path) > 0)
    
            iter = model.get_iter(dest_path)
            dest_dep = model[iter][NAME_COL]
            command = Command.InsertElement(dest_dep, dest_index, entity)
            info = (dest_path, iter, dest_dep, dest_index, model)
            self.main.do_command(command,
                                 lambda: self.refresh_after_insert(info),
                                 lambda: self.refresh_after_delete(info))
        except Exception, e:
            dialog = TextViewDialog.TextViewDialog(
                "Error", "Error during clipboard paste operation:",
                str(e))
            dialog.run()
            dialog.hide()
