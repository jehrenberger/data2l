import types
import gtk
import os
import sys
import data2l_version
import urllib

class StartPageHtmlViewBase(object):
    def __init__(self, xml, main, config,_htmlview):
        self.config = config
        self.main = main        
        self.xml = xml
        self.MainViewSwitch = xml.get_widget('MainViewSwitch')
        self.htmlView = _htmlview
        self.htmlView.show()
        self.MainViewSwitch.insert_page(self.htmlView)
        self.MainViewSwitch.set_current_page(0)

                
    def show(self):
        self.MainViewSwitch.set_current_page(1)
    
    def hide(self):
        self.MainViewSwitch.set_current_page(0)
        
    def toggle(self):
        if self.MainViewSwitch.get_current_page() == 0:
            self.show()
        else:
            self.hide()
                    
    def  OnBeforeNavigateBase(self, dest):
        dum, dum2, command = dest.partition("nd://")
        command = urllib.unquote(command)
        if command[len(command)-1] == "/":
            command = command[:-1]
            
        if   command.find("HideStartPage") == 0:
                self.hide()
        elif command.find("help") == 0:
                self.main.ShowHelp( command[4:].encode('utf8') )
        elif command.find("open_def") == 0:            
                self.main.open_def_file(command[8:])
        elif command.find("NewProject") == 0:
                self.main.newFile(None,None,True)   
        elif command.find("DoNotShowStartPage") == 0:
                self.config.set('html_view_on_start',False)
                self.config.dump()      
                self.hide()
        elif command.find("open_browser:") == 0:
                import webbrowser
                webbrowser.open(command[13:].encode('utf8'))                                      
        
        return True # never follow the link        
        
    def GetStartPageContent(self):
        page =  """<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
 <head>
 <style>
body {
    margin:0px 0px 0px 0px;
    padding:0px 0px 0px 0px; 
    border:0px 0px 0px 0px;
    font-family: Arial, Verdana, Helvetica, sans-serif;
}
ul {
    margin:0px 0px 0px 0px;
    padding:0px 0px 0px 0px; 
    border:0px 0px 0px 0px;
    padding-left:5px;     
}
li { 
    list-style-type:none;
 } 

li a {
    font-size:10px;
 }

a span {
    font-size:8px;
    color:red
}

h3 {
     color: rgb(248,77,0);
     background: #e2e2e2;
     margin-bottom:0px;
     padding-bottom:0px;
     padding-left:5px;     
}

 h1 {
     color: rgb(248,77,0);
     margin-bottom:0px;
     padding-bottom:0px;
     padding-left:5px;     
}

p {
    border:0px 0px 0px 0px;
    margin:0px 0px 0px 0px;
    padding:0px 0px 0px 0px; 
    padding-left:5px;
    font-size:10px;  
}

a {
   color: rgb(10,10,10);
   font-size:12px;
   text-decoration:none;
}
.fill {
    padding:10px 10px 0px 0px; 
}

a:hover {
  text-decoration: underline;
}


 </style>
 </head>
 <body>
     <div style="padding : 5px;margin-top:20px;">
       <div style="float:left;width:70%%;">
           
        <h1 style="padding-bottom:10px;">
         <img  src="http://www.eccam.com/img/eccam-dots.png" style="position:relative;top:12px;"/>
         Data2l 
         <span style="font-size:12px;">
              design &ndash; document &ndash; generate &ndash; validate &ndash; optimize
         </span>
        </h1>
        </div>
        <div style="float:left;text-align:right;width:30%%">
                <p style="padding-bottom:3px;padding-top:19px;">
         <a href="command://open_browser:mailto://data2l@eccam.com"><span><b>&gt;&gt;</b></span> e-mail us</a><br/>
         <a href="command://open_browser:http://www.eccam.com"><span><b>&gt;&gt;</b></span> see www.eccam.com</a>
           </p>
      </div>
       <div style="width:100%%;height:5px;"> </div>
        <p style="color:black;font-size:8px;">Version %(version)s</p>
        <p style="padding-bottom:3px;padding-top:3px;">
         <a href="command://HideStartPage"><span><b>&gt;&gt;</b></span> Skip</a> |          
         <a href="command://DoNotShowStartPage"><span><b>&gt;&gt;</b></span> Do not show this Start Page on Startup</a>
        </p>
        <p style="padding-bottom:3px;padding-top:3px;font-size:14px;">
            <a  href="command://NewProject"><span><b>&gt;&gt;</b></span> Start a New Project</a>   
        </p>
      </div>

               
    </div>
 
 
  <div style="float:left;width:100%%">
  
    <!-- levy sloupec -->
     <div style="float:left;width:49%%;">
     
     <!-- comment this thing out when more than 2 recent definitions -->
     %(welcome_comment_start)s     
     <div >
        <h3> Welcome to Data2l</h3>
    <p style="padding-bottom:3px;">
        We designed Data2l [datatool] to ease management of complicated propriatery 
        read-only databases and flat-files.It helps you to understand new 
        definitions quickly, instantly inspect binary files, see what values 
        are on which places what are the relations etc... 

    </p> 
    <p  style="padding-bottom:3px;">
        You can explore files using python scripts, generate new data files 
        according to definition. In case of really large files generate an optimized 
        C++ code and use it for further fast work with your data. 
        You can write your custom generator for documentation or source code generation.
    </p> 
    <p  style="padding-bottom:3px;">
        Structure  can be defined once and then used for different purposes - validation, 
        documentation generation, DB creation, access code generation etc... and all can 
        benefit from one master definition.
    </p>
    <p>
    <br/>
    I believe it can be of help. Vaclav Opekar, Eccam
</p>
      </div> 
         
      %(welcome_comment_end)s   
      
           
      <div >
        <h3> Recent Projects </h3>
        <ul>
        %(recenly_opened)s
        </ul>
      </div>
    
  
      
<!-- TODO mozna v budoucnu ale inspect file historie se bude muset uklada i spolecne s definici      
      <div >
      
        <h3> Recent File Inspections </h3>
        <ul>
        %(recently_inspected)s
        </ul>  
     </div>
-->
    </div>
    
    <!-- pravy sloupec -->    
     <div style="float:right;width:49%%">
      <div >
        <h3> Definition Examples </h3>
        <ul>
        %(definition_examples)s
        </ul>     
      </div>
      
      <div >
        <h3> Tutorial Topics</h3>
        <ul>
            <li><a href="command://help:html/intro.html#intro"><span><b>&gt;&gt;</b></span> Hello world definition example</a></li>
            <li><a href="command://help:html/domsax.html#domsax"><span><b>&gt;&gt;</b></span> DOM and SAX approach to your data </a></li>
            <li><a href="command://help:html/formatlanguage.html#formatlanguage"><span><b>&gt;&gt;</b></span> Data description language </a></li>
        </ul>          
      </div>
    </div>
      
</div>  
      
</body>
</html>
                """
        recently_opened_defs = ""
        if len (self.config.get("recent_defs")) ==0:
            recently_opened_defs = ' <li> No projects recently opened. </li>'            
            
        else:
            for item in self.config.get("recent_defs"):
                recently_opened_defs += '<li><a href="%s"><span><b>&gt;&gt;</b> </span> %s </a></li>' % (
                                                "command://open_def"+item, 
                                                item)
        example_defs = ""

        for item in self.main.get_example_defs_list():
            example_defs += '<li><a href="%s"><span><b>&gt;&gt;</b> </span> %s </a></li>' % (
                                            "command://open_def"+urllib.quote(item), 
                                            item[item.rfind(os.sep)+1:])
            
        
            
#        sys_path = os.path.join(os.path.dirname(sys.argv[0]), "images").replace("\\","/")  # .replace(" ","%%20") #IE accepts spaces FF not            
#        page = page.replace("$IMG_PATH$", "file://"+sys_path)
        
        repl_map = {}
        repl_map["recenly_opened"] = recently_opened_defs
        repl_map["recently_inspected"] = ""
        repl_map["definition_examples"] = example_defs  
        repl_map["version"] = data2l_version.data2l_version
        
        if len (self.config.get("recent_defs"))  > 3:
            repl_map["welcome_comment_start"] = '<!-- '
            repl_map["welcome_comment_end"] = ' -->'
        else:
            repl_map["welcome_comment_start"] = "" 
            repl_map["welcome_comment_end"] = "" 
                                   
        return (page % repl_map ).encode('utf8')
  