import DataTypes as DT
import BasicTypes as BT
from DefWalk import DefinitionWalker
import Definition
import DefWalk
from Utility import xassert, print_trace


__all__ = """\
SC_MISSING_OFFSET
SC_MISSING_OFFSET_ALT
SC_DUPLICATE_NAMES
SC_DUPLICATE_STRUCT_NAMES
SC_ATTRIB_MISSING_INIT
SC_ALTERNATIVE_MISSING_SELECTOR
SC_MISSING_ARRAY_SIZE
StructCheck
""".split()


SC_MISSING_OFFSET = 1
SC_MISSING_OFFSET_ALT = 2
SC_DUPLICATE_NAMES = 3
SC_DUPLICATE_STRUCT_NAMES = 4
SC_ATTRIB_MISSING_INIT = 5
SC_ALTERNATIVE_MISSING_SELECTOR = 6
SC_MISSING_ARRAY_SIZE = 7


## This class implements checking of semantic correctness
# of structure definitions.
class StructCheck(DefWalk.IDefWalkEvents):
    def __init__(self):
        self.definition = None
        self.problems = []
        self._visited = None
        self._name2instances = None

    def check(self, definition):
        xassert(isinstance(definition, Definition.Definition))
        self.definition = definition
        self._visited = set()
        self._name2instances = {}
        self.problems = []

        DefinitionWalker(self, self.definition).traverse()

        for name, instances in self._name2instances.iteritems():
            if len(instances) > 1:
                self.problems.append((SC_DUPLICATE_STRUCT_NAMES,
                                     (name, instances)))

        self._name2instances = None
        self._visited = None
        self.definition = None
        return

    def elementBegin(self, struct):
        if struct in self._visited:
            return
        else:
            self._visited.add(struct)

        aggs = struct.getAggregates()
        deps = struct.getDeps()
        xassert(len(aggs) == len(deps))
        if isinstance(struct, DT.Struct):
            for i, (elem, dep) in enumerate(zip(aggs, deps)):
                len_deps = len(deps)
                if isinstance(elem, DT.Array):
                    # Check that element that follows array without
                    # in_memory==True has its offset defined.
                    memory_field = False
                    if len(elem.getDeps()) == 1:
                        if elem.getDeps()[0].in_memory:
                            memory_field = True

                    if (not memory_field and i+1 < len_deps
                        and deps[i + 1].offset is None):
                        # Look inside Alternative if it itself does not have
                        # the offset defined.
                        if isinstance(aggs[i + 1], DT.Alternative):
                            alt_aggs = aggs[i + 1].getAggregates()
                            alt_deps = aggs[i + 1].getDeps()
                            # Check each element of Alternative.
                            for alt_i, alt in enumerate(alt_aggs):
                                if (not isinstance(alt, DT.Void)
                                    and alt_deps[alt_i].offset is None):
                                    self.problems.append((SC_MISSING_OFFSET_ALT,
                                                          (struct, elem,
                                                           aggs[i + 1], alt)))
                        # Or just check non-Alternative.
                        else:
                            self.problems.append((SC_MISSING_OFFSET,
                                                  (struct, elem,
                                                   aggs[i + 1])))

        if isinstance(struct, DT.Composite):            
            name2instances = {}
            for i, (elem, dep) in enumerate(zip(aggs, deps)):
                # Scan for duplicate names inside single structure.
                insts_set = name2instances.setdefault(elem.getName(), set())
                insts_set.add(elem)
                # Check that attributes have init filled.
                if isinstance(elem, DT.Attrib) and dep.init is None:
                    self.problems.append((SC_ATTRIB_MISSING_INIT,
                                          (struct, elem.getName())))
                # Check that alternatives have selector filled.
                if isinstance(elem, DT.Alternative) and dep.selector is None:            
                    self.problems.append((SC_ALTERNATIVE_MISSING_SELECTOR,
                                  (struct, elem.getName())))
                # Check that arrays have size defined and positive.
                if (isinstance(elem,DT.Array) or 
                    (isinstance(elem,DT.Basic) and isinstance(elem.basic_type, BT.StaticSizeArray))):
                    if dep.size is None:
                        self.problems.append((SC_MISSING_ARRAY_SIZE, 
                                     (struct, elem.getName())))
                                        
            # Append the detected problems into problems list.
            for name, instances in name2instances.iteritems():
                if len(instances) > 1:
                    self.problems.append((SC_DUPLICATE_NAMES,
                                          (struct, name, instances)))

            # Collect names and instances for detectoin of duplicate names
            # in whole definition.
            insts = self._name2instances.setdefault(struct.getName(), set())
            insts.add(struct)        

        return

    def results(self):
        return self.problems
