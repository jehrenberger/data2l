import DataTypes as DT
from Utility import xassert, impossible
from DefWalk import DefinitionWalker, IDefWalkEvents


class DocsGen(IDefWalkEvents):
    def __init__(self):
        self._definition = None
        self._visited = None

    def generate(self):
        xassert(self._definition is not None)
        self._visited = set()
        DefinitionWalker(self, self._definition).traverse()
        self._visited = None
        return

    def dependencyBegin(self, dep):
        elem = dep.getElement()
        if isinstance(elem, DT.SimpleElement):
            return

        elem_name = elem.getName()
        if elem_name in self._visited:
            return
        else:
            self._visited.add(elem_name)

        self.decl_composite_begin(dep)
        self.decl_composite(dep)
        self.decl_composite_end(dep)
        return

    def set_definition(self, definition):
        self._definition = definition

    def get_definition(self):
        return self._definition

    def decl_composite(self, dep):
        self.decl_composite_header(dep)
        self.separator_header_body()
        self.decl_composite_body(dep)
        return

    def decl_composite_begin(self, dep):
        pass

    def decl_composite_end(self, dep):
        pass

    def decl_composite_header(self, dep):
        pass

    def decl_composite_body(self, dep):
        composite = dep.getElement()
        aggs = composite.getAggregates()
        deps = composite.getDeps()
        xassert(len(aggs) == len(deps))
        for el, dep in zip(aggs, deps):
            if isinstance(el, DT.SimpleElement):
                self.use_simple(dep)
            elif isinstance(el, DT.Composite):
                self.use_composite(dep)
            else:
                impossible()
            self.separator_body_entities()
        return

    def use_composite(self, dep):
        self.use_composite_name(dep)
        self.separator_name_type()
        self.use_composite_type(dep)

    def use_composite_name(self, dep):
        pass

    def use_composite_type(self, dep):
        pass

    def use_simple(self, dep):
        self.use_simple_name(dep)
        self.separator_name_type()
        self.use_simple_type(dep)

    def use_simple_name(self, dep):
        pass

    def use_simple_type(self, dep):
        pass

    def separator_name_type(self):
        pass

    def separator_header_body(self):
        pass

    def separator_entities(self):
        pass

    def separator_body_entities(self):
        pass
