## @package gtkGUI.PropDialog
#  Create property dialog according to given list of tuples.

import gtk
import sys
from gtkGUI.Config import config
from gtkGUI import FileChooser, DirectoryChooser
import os.path

__all__ = """\
PropertiesDialog
""".split()

rcstring = """
style "checkbutton-style" {
    GtkCheckButton::indicator_spacing = 0
}

class "GtkCheckButton" style "checkbutton-style"
"""
gtk.rc_parse_string(rcstring)

## Dialog for editing properties
#  @see PropDialogExample
class PropertiesDialog(object):
    def __init__(self, properties, title, parent = None):
        self.properties = properties
        if title is None:
            title = "Properties"
        self.title = title
        self.__gladefile = os.path.join(os.path.dirname(__file__), "gui.glade")
        xml = gtk.glade.XML(self.__gladefile,'PropertiesDialog')
        self.dialog = xml.get_widget('PropertiesDialog')
        self.dialog.set_title(self.title)
        if parent is not None: self.dialog.set_parent(parent)
        scrolled_window = xml.get_widget('scrolledwindow2')
        table = xml.get_widget('table1')
        
        rows_num = len(properties)
        # now use layout
        table.resize(rows_num, 3)
        #table = gtk.Table(rows_num, 3)
        table.set_border_width(10)
        table.set_row_spacings(10)
        table.set_col_spacings(10)
        
        for i,prop in enumerate(self.properties):
            label = gtk.Label(prop[1][0])
            label.set_alignment(0, 0.5) # align left
            table.attach(label, 0,1, i,i+1, gtk.FILL,0)
            # select type of edit widget
            if prop[1][2] == "string":
                entry = gtk.Entry()
                entry.set_text(prop[1][1])
                entry.connect("focus-out-event", self.entry_changed, i)
                table.attach(entry, 1,3, i,i+1, gtk.FILL | gtk.EXPAND,0)
            elif prop[1][2] == "bool":
                check = gtk.CheckButton()
                check.set_active(prop[1][1])
                check.connect("clicked", self.check_clicked, i)
                alignment = gtk.Alignment(0,0,0,0)
                alignment.add(check)
                table.attach(alignment, 1,3, i,i+1, gtk.FILL,0)
            elif prop[1][2] == "int":
                adj = gtk.Adjustment(1.0, -sys.maxint, sys.maxint, 1.0, 5.0, 0.0)
                spin = gtk.SpinButton(adj)
                spin.set_numeric(True)
                spin.set_value(prop[1][1])
                spin.connect("focus-out-event", self.spin_changed, i)
                alignment = gtk.Alignment(0,0,0,0)
                alignment.add(spin)
                table.attach(alignment, 1,3, i,i+1, gtk.FILL,0)
            elif prop[1][2] == "dir" or prop[1][2] == "file":
                entry = gtk.Entry()
                entry.set_text(prop[1][1])
                entry.connect("focus-out-event", self.entry_changed, i)
                table.attach(entry, 1,2, i,i+1, gtk.FILL | gtk.EXPAND,0)
                button = gtk.Button("...")
                button.connect("clicked", self.button_clicked, prop[1][2],entry, i)
                table.attach(button, 2, 3, i, i+1, 0,0)
            
        table.show_all()
        #scrolled_window.add_with_viewport(table)        
                
    def check_clicked(self, widget, row):
        self.properties[row] = (self.properties[row][0],(self.properties[row][1][0],
                                                         widget.get_active(),
                                                         self.properties[row][1][2]))
        
    def entry_changed(self, widget, event, row):
        self.properties[row] = (self.properties[row][0], (self.properties[row][1][0],
                                                          widget.get_text(),
                                                          self.properties[row][1][2]))
    
    def spin_changed(self, widget, event, row):
        self.properties[row] = (self.properties[row][0], (self.properties[row][1][0],
                                                          widget.get_value_as_int(),
                                                          self.properties[row][1][2]))
        
    def button_clicked(self, widget, type, entry, row):
        # open path dialog
        if (type == "file"):
            chooser = FileChooser.FileChooser(self.dialog, "Select file", None, 
                                              entry.get_text(), 0)
        else:
            chooser = DirectoryChooser.DirectoryChooser(self.dialog, "Select directory",
                                                        entry.get_text()) 
        
        if chooser.run() == gtk.RESPONSE_OK:
            entry.set_text(chooser.get_filename())
            self.properties[row]=(self.properties[row][0], (self.properties[row][1][0],
                                                            chooser.get_filename(),
                                                            self.properties[row][1][2]))
        chooser.destroy()
    def run(self):
        response = self.dialog.run()
        return response
    
    def hide(self):
        self.dialog.hide()

def PropDialogExample():
    editable_props = [("key1", ("Path", "C:\\", "dir")),
                      ("key2", ("Check box", True, "bool")),
                      ("key3", ("String variable ", "some text", "string")),
                      ("key4", ("Check box", False, "bool")),
                      ("key5", ("Integer variable", 5, "int")),
                      ("key6", ("File", "C:\example.bas", "file")),
                      ("key7", ("Path", "C:\\", "dir"))]
                
    propDial = PropertiesDialog(editable_props, "Properties Dialog Example")
    ret = propDial.run()
    if ret == gtk.RESPONSE_OK:
        print "Data will be changed"
    else:
        print "Data won't be changed"        
        
if __name__ == "__main__":
    PropDialogExample()
    import gobject
    gobject.idle_add(gtk.main_quit) 
    gtk.main()
