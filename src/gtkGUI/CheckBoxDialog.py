import gtk, gobject
import os.path

__all__ = """\
CheckBoxDialog
""".split()

class CheckBoxDialog(object):
    def __init__(self,title,list):
        self.check_list = list
        self.title = title

        self.__gladefile = os.path.join(os.path.dirname(__file__), "gui.glade")
        xml = gtk.glade.XML(self.__gladefile,'ListViewDialog')
        self.dialog = xml.get_widget('ListViewDialog')
        self.dialog.set_title(self.title)
        self.list = xml.get_widget('listView')
        self.list_store = gtk.ListStore(gobject.TYPE_BOOLEAN, str, str, str)

        toggle_cell = gtk.CellRendererToggle()
        toggle_cell.set_property('activatable', True)
        toggle_cell.connect( 'toggled', self.toggled_cb, self.list_store )
        toggle_col = gtk.TreeViewColumn('', toggle_cell)
        toggle_col.add_attribute(toggle_cell, "active", 0)
        self.list.append_column(toggle_col)

        value_cell = gtk.CellRendererText()
        value_col = gtk.TreeViewColumn('Element', value_cell)
        value_col.add_attribute(value_cell, 'text', 1)
        self.list.append_column(value_col)

        value_cell = gtk.CellRendererText()
        value_col = gtk.TreeViewColumn('Getter', value_cell)
        value_col.add_attribute(value_cell, 'text', 2)
        self.list.append_column(value_col)

        value_cell = gtk.CellRendererText()
        value_col = gtk.TreeViewColumn('Getter Text', value_cell)
        value_col.add_attribute(value_cell, 'text', 3)
        self.list.append_column(value_col)

        for i in self.check_list:
            self.list_store.append([i[0], i[1], i[2], i[3]])
        self.list.set_model(self.list_store)

    def toggled_cb( self, cell, path, model ):
        model[path][0] = not model[path][0]
        return

    def run(self):
        response = self.dialog.run()
        for i,item in enumerate(self.list_store):
            self.check_list[i][0] = item[0]

        return response

    def hide(self):
        self.dialog.hide()
