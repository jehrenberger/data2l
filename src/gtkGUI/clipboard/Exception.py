__all__ = """\
ClipboardException
""".split()


## Helper class for wrapping native exceptions to shield away implementation
# details of clipboard processing.
class ClipboardException(Exception):
    def __init__(self, original_exception):
        self.original = original_exception

    def __str__(self):
        return str(self.original)
