import generators.Generator as GEN
from generators.GetterGen import *
from Cheetah.Template import Template
from generators.GenUtils import *
import DataTypes
from gtkGUI.Config import config, save_dirname_into

import os

class GenGetter:
    def generate(self,dep,vg):
        self.dep = dep
        self.str="";
        self.vg = vg
        vg.walk(self._walk_value_getters)
        return self.str
    
    
    def getIndexStr(self, dep):
        dd = dep
        str_index = ""
        while dd is not None:
            if dd.in_memory and isinstance(dd.getParent().getType(), Array):
                str_index =  ".vec_" +dd.getName()+ "[" + self.getIndexStr(dd.getParent()) +".actIndex]"+str_index
            else:
                str_index =  "." + dd.getName()+str_index
            if dd.hasProperty("cpp_ro_block"):
                       break
            dd = dd.getParent()
        return str_index.lstrip(".")
        
    ##
    #  dep is the dependency which askes the result of value getter
    #  vg is the RelativeValue getter which has the path to thr right walue in the tree
    #
    def codeFromRelativeVG(self, dep, vg):
        xassert(isinstance(vg,VE.RelativeValue))
        rvg_str=""
        d = vg.getDOM(self.dep)
  
        return self.getIndexStr(d)

    def _walk_value_getters(self, getter, sub_getters):
        if  isinstance(getter, VE.BinaryOp):
            self.str += "("
            sub_getters[0].walk(self._walk_value_getters)
            self.str += " %s " % (getter.getOperatorStr())
            sub_getters[1].walk(self._walk_value_getters)
            self.str += ")"

        if  isinstance(getter, VE.UnaryOp):
            self.str += " %s " % (getter.getOperatorStr())
            sub_getters[0].walk(self._walk_value_getters)

        if  isinstance(getter, VE.RelativeValue):
                        self.str += self.codeFromRelativeVG(self.dep,getter)

        if  isinstance(getter, VE.Const):
            v = getter.getDOMValue(None)
            if isinstance(v,str):
                self.str += "\"%s\"" % (getter.getDOMValue(None))
            else:
                self.str += "%d" % (getter.getDOMValue(None))

        if  isinstance(getter, VE.SizeOf):
            sizeof = getter.target_getter.getDOM(self.dep).getSizeOf()
            if sizeof <= 0:
                raise GeneratorException((self.dep.getName(), self.vg.getText()))
            self.str += "%d /*sizeof(%s)*/"  % (sizeof, self.dep.element.name)

        if  isinstance(getter, VE.OffsetOf):
            sub_getters[0].walk(self._walk_value_getters)
            self.str += ".offset"


        if  isinstance(getter,   VE.ArraySizeOf):
            sub_getters[0].walk(self._walk_value_getters)
            self.str += ".arraySize"

        if  isinstance(getter,   VE.ArrayActIndexOf):
            sub_getters[0].walk(self._walk_value_getters)
            self.str += ".actIndex"

        if  isinstance(getter, VE.OwnIndexOf):
            raise GeneratorException((self.dep.getName(), self.vg.getText() + "no ownindex support"))

        if  isinstance(getter, VE.IfThenElse):
            self.str += "/*if*/("
            sub_getters[0].walk(self._walk_value_getters)
            self.str += " ) ? \n/*then*/ "
            sub_getters[1].walk(self._walk_value_getters)
            self.str += ": /*Else*/"
            sub_getters[2].walk(self._walk_value_getters)

        if  isinstance(getter, VE.StrIndex):
            self.str += "data2l::strindex("
            sub_getters[0].walk(self._walk_value_getters)
            self.str += ", "
            sub_getters[1].walk(self._walk_value_getters)
            self.str += ")"

        if  isinstance(getter, VE.ReadArrayIndex):
            getter.array_extr.walk(self._walk_value_getters)

            target_array = getter.array_extr.getDOM(self.dep)

            if len(getter.tail_extr.path) > 0:
                self.str += ".vec_"  + getter.tail_extr.path[0].step(target_array).getName()
            
            self.str += "["
            getter.index_extr.walk(self._walk_value_getters)
            self.str += "]"
          
           
            first = len(getter.tail_extr.path) > 1
            
            tail = target_array
            for i in getter.tail_extr.path:
                tail=i.step(tail)
                if first:
                    first= False
                else:
                    self.str += ".%s" % (tail.element.name)
  

        if  isinstance(getter, ExprVE.ExprVE):
            getter.compiled_getter.walk(self._walk_value_getters)        

common_functions = """#import DataTypes
#import BasicTypes
#from generators.Generator import GeneratorException
##################################
#def pick_var_int_type($dep)
 #set $var_size = $dep.getSizeOf()
 #if $var_size != -1
  #if $var_size == 1
Byte#slurp
  #elif $var_size == 2
Word#slurp
  #elif $var_size <= 4
DWord#slurp
  #else
LongLong#slurp
  #end if
 #else
LongLong#slurp
 #end if
#end def
#####################
"""

class CppGen_ReadOnly(GEN.Generator):
    def SetUnderBlock(self,dep):
        temp_dep = dep
        while temp_dep.getParent() is not None:
            if temp_dep.hasProperty("cpp_ro_block"):
                if temp_dep != dep:
                    dep.addProperty("under_block", temp_dep)
                    break
            temp_dep = temp_dep.getParent()
            
    def generate(self, root_dep, enumsHolder, settings):
        classes=[]
        already = {}
        for i in listFromDepTree(root_dep):
            self.SetUnderBlock(i)
            if i.hasProperty("under_block") and i.getParent() is not None and \
                   isinstance(i.getParent().getType(), DataTypes.Array):
                    if not i.in_memory:
                        print "setting in_memory ", i.getName()
                        i.in_memory = True
            classes.append(i)
            #if i.isComposite() and not already.has_key(i.getType()):
            #    classes.append(i)
            #    already[i.getType()] = 1

        target_dir = settings["TargetDirectory"]

        #if the directory is not available create it
        if not os.access(target_dir, os.F_OK):
                os.makedirs( target_dir)

        h =  open(target_dir + settings["OutputFilenameH"], 'w')
        tmpl = Template(common_functions+"""#
####
#raw
/**
 *  Generated by Data2l do not change.
 *  www.eccam.com/datatool.php
 *
 **/
#include "types.h"
#include "reader.h"
#include "dbstring.h"

#include <vector>
#include <map>
#end raw
###
#def  getArrayStaticSize($d)
#if  $d.getProperty("static_size") is not None:
    #return $d.getProperty("static_size")#slurp
#else
#try
    #return $d.size.getDOMValue($d)#slurp
#except
#set ret = "Element '"+ $d.getParent().getName() +"."+$d.getName() + "' has variable size.\\n\\n" 
#set ret = $ret + "Generator is not able to deduce the static size of C++ variable.\\n"
#set ret = $ret + "Please specify static size using 'static_size' custom property.\\n\\n"
#set ret = $ret + "Or use a different code generator which generates code with dynamic sized arrays.\\n"
    #raise GeneratorException($ret)
#end try    
#end if    
#end def
#def  definition($e)
#if isinstance($e.getType(), DataTypes.Void)
void()  #slurp
#elif isinstance($e.getType(), DataTypes.Basic)
#if isinstance($e.getType().basic_type, BasicTypes.String)
data2l::String<$getArrayStaticSize($e)> #slurp
#elif isinstance($e.getType().basic_type, BasicTypes.VarInteger)
$pick_var_int_type($e)
#elif isinstance($e.getType().basic_type, BasicTypes.StaticSizeArray)
data2l::Block<$getArrayStaticSize($e), $e.getType().basic_type.basicElement> #slurp
#else
${e.getType().basic_type} #slurp
#end if
#else
C$e.getName() #slurp
#end if
#end def
######################
#def member_def($m)
#if $m.in_memory
std::vector<$definition($m)> vec_$m.getName();
#else
$definition($m) $m.getName();
#end if
#end def
######################
#def  print_class($c)
struct C$c.getName() {
    OffsetType offset;
#if isinstance($c.getType(), DataTypes.Alternative)
    int validAlternative;
#elif  isinstance($c.getType(), DataTypes.Array)
    SizeType actIndex;
    SizeType arraySize;
#end if
#for $i in $c.getAggregates()
#if not $i.hasProperty("cpp_ro_block") and not isinstance($i.getType(), DataTypes.Void )
    $member_def(i)#slurp
#end if    
#end for
};
#end def
#######################
#for $i in filter( lambda d: d.rep_count== 1, $classes)
$print_class($i)
#end for

//main file initialization function
void init$root_dep.getName()(data2l::Reader& r, C$root_dep.getName()& root);

//state changers (anything above blocks) (for arrays)
#for $i in $classes
#if isinstance($i.getType(), DataTypes.Array) and not $i.hasProperty("under_block")
#if not ( len($i.getAggregates()) == 1 and $i.getAggregates()[0].in_memory) 
void loadIndex_${i.getName()}(data2l::Reader& r, C$root_dep.getName()& root, SizeType index);
#end if
#end if
#end for

//block readers
#for $i in  filter( lambda d: d.rep_count== 1 and d.hasProperty("cpp_ro_block"),   $classes)
void $i.getProperty("cpp_ro_block")(data2l::Reader& r, C$i.getName()& $i.getName(), #slurp
const C$root_dep.getName()& root);
#end for

""")
        tmpl.classes = classes
        tmpl.settings = settings
        tmpl.root_dep = classes[len(classes)-1]
        h.write(str(tmpl))
        h.close()
        
        cpp =  open(target_dir  + settings["OutputFilenameCPP"], 'w')
        tmpl = Template(common_functions+"""#
#import DataTypes
#import BasicTypes
####
#set $hash="#"
${hash}include "$settings["OutputFilenameH"]"
#def init($i, $block = False)
#set id=""
#if $root_dep != $i and not $block
static #slurp
#set id=$str($i.rep_count)
#end if
#if $block
void $i.getProperty("cpp_ro_block")#slurp
#else
void init${id}${i.getName()}#slurp
#end if
(data2l::Reader& r, C$i.getName()& $i.getName()#slurp
#if $root_dep != $i
, const C$root_dep.getName()& $root_dep.getName()#slurp
#end if
#if $i.hasProperty("under_block")
, const C${i.getProperty("under_block").getName()}& $i.getProperty("under_block").getName() #slurp
#end if
 )
{
   SizeType _size;
#if $block
   ${i.getName()}.offset = r.tell();   
#else
#if $i.offset is not None
   ${i.getName()}.offset = $getter.generate($i,$i.offset);
   r.seek(${i.getName()}.offset);
#else
   ${i.getName()}.offset = r.tell();   
#end if
#end if
##### ARRAY prelude 
#if isinstance($i.getType(), DataTypes.Array)
   ${i.getName()}.arraySize = $getter.generate($i,$i.size);
   ${i.getName()}.actIndex = 0;
   
 #for $a in filter(lambda d: d.in_memory, $i.getAggregates())
   ${i.getName()}.vec_${a.getName()}.resize(${i.getName()}.arraySize);
 #end for
#end if ##### ARRAY prelude till here
##### ALTERNATIVE prelude
#if isinstance($i.getType(), DataTypes.Alternative)
   ${i.getName()}.validAlternative = $getter.generate($i,$i.selector);
   switch ( ${i.getName()}.validAlternative )
   {
#end if ##### ALTERNATIVE prelude till here

#for $idx, $a in enumerate($i.getAggregates())
#if isinstance($i.getType(), DataTypes.Alternative)
   case $idx:
#end if
#set $aggName = $i.getName()+"."+$a.getName()
#if $a.in_memory and isinstance($i.getType(), DataTypes.Array)
  for (int i=0;i<${i.getName()}.arraySize; i++)
  {
      ${i.getName()}.actIndex = i;
#set $aggName = $i.getName() + ".vec_" + $a.getName() + "[i]"
#end if      
#if $a.isComposite() and not $a.hasProperty("cpp_ro_block")   
      init${a.rep_count}${a.getName()}(r, $aggName , $root_dep.getName() #slurp
#if $a.hasProperty("under_block")
, $a.getProperty("under_block").getName() #slurp
#end if      
      );
#elif isinstance($a.getType(), DataTypes.Basic)
#set var = "1";
#if $a.size is not None
    _size = $getter.generate($a,$a.size);
    #set var = "_size"
#end if    

#if isinstance($a.element.basic_type, BasicTypes.BasicArrayType)
    r.read${a.element.basic_type.basicElement}($aggName , $var);
#if isinstance($a.element.basic_type,BasicTypes.String)
    ${aggName}[$var]=0;
#end if
#elif isinstance($a.element.basic_type, BasicTypes.VarInteger)
   $aggName = 0;
   r.readBin(reinterpret_cast<char *>(& $aggName),_size);
#else
   r.read${a.getType().basic_type}(& $aggName ,1);
#end if   
#end if ##instance basic type
#if $a.in_memory and isinstance($i.getType(), DataTypes.Array)
    }
#end if
#if isinstance($i.getType(), DataTypes.Alternative)
   break;
   
#end if
#end for
   #if isinstance($i.getType(), DataTypes.Alternative)
   }
   #end if
}
####end if
#end def

#for $i in $classes
#if not $i.hasProperty("cpp_ro_block")
$init($i)
#end if
#end for

//state setters
#for $i in $classes
#if isinstance($i.getType(), DataTypes.Array) and not $i.hasProperty("under_block")
void loadIndex_${i.getName()}(data2l::Reader& r, C$root_dep.getName()& $root_dep.getName(), SizeType index)
{
#set $prefix = ""
#set $dep = $i
#while $dep is not None
#set $prefix = $dep.getName() + "." + $prefix
#set $dep = $dep.getParent()
#end while
    assert(index < ${prefix}arraySize);
    ${prefix}actIndex = index;
    
#for $a in $filter(lambda d: not d.in_memory, $i.getAggregates())
#if $a.isComposite() and not $a.hasProperty("cpp_ro_block")
    init${a.rep_count}${a.getName()}(r, ${prefix}${a.getName()} ,$root_dep.getName());
#end if
#end for    
}
#end if
#end for

//block readers
#for $i in  filter( lambda d: d.rep_count== 1 and d.hasProperty("cpp_ro_block"),   $classes)
$init($i, True)
#end for


""")
        tmpl.classes = classes
        tmpl.settings = settings
        tmpl.root_dep = classes[len(classes)-1]        
        tmpl.getter = GenGetter()
        cpp.write(str(tmpl))
        cpp.close()        


    def getDefaultSettings(self,root_dep):
        def_settings = [("TargetDirectory",
                            ("Target directory", config.get('cpp_gen_directory'), "dir")),
                        ("OutputFilenameH",
                            (".h file name", root_dep.getElement().getName() + "_ro.h", "string" )),
                        ("OutputFilenameCPP",
                            (".cpp file name",root_dep.getElement().getName() + "_ro.cpp", "string" ))]
        return def_settings

    def getMenuString(self):
        return "C++ Generator for read only access"

    def getDescription(self, lang):
        return "C++ Generator for read only access"




generator = CppGen_ReadOnly()
