from Cheetah.Template import Template
from templ.HtmlTemplate import *
from DocGenLogic import *
import re
from HtmlStyle import htmlStyle


        
class HtmlDocGen:
    """ This class implements the generator of the file 
        structure documentation in html format.
        
        @see generateToFile, generate  are main method
    """
    def removeEmptyTags(self, html):
        regexp = '<[a-z]+ ([^<>])*>\s*</[a-z]*>'       
        p = re.compile(regexp)        
        return p.sub( "", html) 

        
    def generateToFile(self, definition, filename):
        file = open(filename, 'w')
        html = self.generate(definition)
        file.write(html)
        file.close()   
        return
    
    
    def generate(self, definition, fs = 1):
        
        dependency = definition.dep_root
        queue = [dependency]
        logic = DocGenLogic()
        utils = DocGenUtils()
        searchList=[{'queue' : queue, 'logic' : logic, 
                     'dep' : dependency, 'utils' : utils}]
        contents = str(Template(templateContents, searchList))

        dependency = definition.dep_root
        queue = [dependency]
        searchList=[{'logic' : logic, 'dep' : dependency, 'utils' : utils}]
        tree = str(Template(templateTree, searchList))
        tree = self.removeEmptyTags(tree)

        dependency = definition.dep_root
        queue = [dependency]
        searchList=[{'queue' : queue, 'logic' : logic, 
                     'dep' : dependency, 'contents' : contents+tree, 
                     'style' : str(Template(htmlStyle,{'h1': int(30*fs), 
                                                       'h2':int(16*fs), 
                                                       'h3': int(12*fs), 
                                                       'normal':int(12*fs), 
                                                       'small':int(10*fs)})), 
                     'utils' : utils}]

        return str(Template(template, searchList))

                
