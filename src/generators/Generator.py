from Utility import pure_virtual


__all__ = """\
Generator
GeneratorException
""".split()

class GeneratorException(Exception):
    def __init__(self,message):
        self.message = message
    def __str__(self):
        return self.message


## Base of all Generator classes.
# Each generater has to implement these methods.
class Generator(object):
    ##  \brief Will write generated and accompanied classes to
    #   a specified folder
    #   \param root_dep is dependecy representing  top pomst structure of the description
    #   \param setting is a property dictionary where key is a name of property and value
    #   is the value  of the propperty
    def generate(self, root_dep, enumsHolder, setting):
        pure_virtual()

    ## \return  property dictionary where key is the name of the property and
    #             the value is a tuple of (default value, property type)
    #             property type can be "string", " checkbox" , "directory"
    def getDefaultSettings(self, root_dep):
        pure_virtual()

    ## string for menu
    def getMenuString(self):
        pure_virtual()

    ## Description string ... necessary??
    def getDescription(self, lang):
        return "No description"
