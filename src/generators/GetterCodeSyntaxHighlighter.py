import re
from exprs.Eval import Context

class GetterCodeSyntaxHighlighter:
    
    def __init__(self):
        self.functions = ""
        for fN in Context.funcs.keys():
            if len(self.functions) > 0:
                self.functions+="|"
            self.functions+=fN                        
    
    def highlightKeywords(self, code):
        code = re.sub(r'(\s)(if|then\s+if|then|else\s+if|else)(\s)',r'\1<span class="keyword">\2</span>\3', code)
        return code 

    def highlightNumbers(self, code):
        return re.sub(r'([^a-zA-Z])((0x[a-fA-F0-9]+)|([0-9]+))',r'\1<span class="numeric">\2</span>', code)

    def highlightFunctions(self, code):
        return re.sub(r'([^a-zA-Z0-9])('+self.functions+')([(])',r'\1<span class="function">\2</span>\3', code)
    
    def highlight(self, code):
        code = " " + code
        code = code.replace("\n","<br/>")
        code = self.highlightNumbers(code)
        code = self.highlightFunctions(code)
        code = self.highlightKeywords(code)
        return code[1:]
            