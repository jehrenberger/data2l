from Preprocessing import Preprocessing
from Parser import *
from DependencyTree import *
from generators.D2LGenerator import D2LGenerator
import time

if __name__ == "__main__":
    
    input = open("/tmp/BaseFile_Ales.d2l", 'r')
    source = input.read()
    input.close()
    
    
    
    _source="""


struct B:
    struct D
    struct E
    byte x

# struct A
# hlavni struktura
root struct A:
    struct B:
        property something-stupid: 55
        init: 3
    
    struct C
    # komentar u deklarace
    struct F

struct D:
    # komentar u deklarace
    struct F
    
    # proste byte
    byte x

struct E:
    # komentar u definice
    struct F:
        byte x:
            init : 7
    byte x:
        property size : 25

struct C:
    byte x


"""
    
    
        
    source_o="""

#comment 1.1
#comment 1.2

root struct Root:
        
        #comment: 2.1
        #comment 2.2:
           
        inmemory struct Struct:
                onsave : doSomething(value)
                init:

                    12

                    7
                string str
                array Array
        
        byte Byte2
        
        #comment 3.1         
        #comment 3.2
        string String:
                selector   : if x > 7 then:
                 5
                  else
                 6
                # je to zidle? 
                property zidlovitost: true
   
array Array:
    dword length
    
   
""" 

    
    #print pp.output
    
    #print "Comments:"
    
    #for comment in pp.comments.keys():
    #    print str(comment) +": \n"+ pp.comments[comment]

    #print "\nCode:"
    
    #for code in pp.codes.keys():
    #    print str(code) +": \n"+ str(pp.codes[code])
            
    
    
    t = time.time()
    
    try:
        pp = Preprocessing(8)    
        pp.transform(source)

        parser = Parser()
        forest = parser.parse(pp.output, pp.comments, pp.codes)
        #print str(forest)

        tree = DependencyTree(forest, pp.comments, pp.codes)
        #print tree.depsList()

        print "Done in: "+ str(time.time() - t)+"s"

        settings = {"OutputDirectory": '/tmp', "OutputName" : 'test.out', "TopRoot" : True, "Comments": True}            
        gen = D2LGenerator()
        gen.generate(tree.rootDependency, settings)


    except ParseException as e:
        print str(e)
        
    except SemanticException as e:
        print str(e)
        


    












