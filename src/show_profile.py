import sys
import pstats


def main():
    if len(sys.argv) < 2:
        print "Syntax:", sys.argv[0], "profile.prof"
        sys.exit(1)

    p = pstats.Stats(sys.argv[1])
    #p.strip_dirs().sort_stats('cumulative').print_stats()
    #p.sort_stats('cumulative').print_stats()
    #p.strip_dirs().sort_stats('time', 'cum').print_stats()
    p.strip_dirs().sort_stats('cum','time').print_stats()


if __name__ == "__main__":
    main()
