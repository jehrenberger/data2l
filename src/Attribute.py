import ValueExtractors as VE
from Utility import pure_virtual, xassert, xassert_type


__all__ = """\
AttributeBase
Attribute
""".split()


class AttributeBase(object):
    def __init__(self):
        pass

    def __str__(self):
        return "AttributeBase"

    ## Override to get attribute type specific way of computing the value.
    def compute(self, dom):
        pure_virtual()

    ## Override to get attribute type specific way of getting the value.
    def value(self):
        pure_virtual()


## This class computes the \a value once and stores it for later use.
class Attribute(AttributeBase):
    def __init__(self, getter):
        AttributeBase.__init__(self)
        xassert_type(getter, VE.ValueGetter)
        self._value = None
        self._getter = getter

    def __str__(self):
        return "Attribute: " + str(self._value)

    ## Accessor for getter.
    def getter(self):
        return self._getter

    ## Computes the attribute's value and store it for later use.
    def compute(self, dom):
        #print "Attribute.compute before:", str(self)
        self._value = self._getter.getDOMValue(dom)
        #print "Attribute.compute after:", str(self)
        xassert(self._value is not None)

    ## Returns value computed in \a compute() method.
    def value(self):
        xassert(self._value is not None)
        return self._value

    def clone(self):
        return Attribute(self._getter)
