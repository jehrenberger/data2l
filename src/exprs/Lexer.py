#
# This file was automatically generated.

# Start state names
INITIAL = 0


# action 1 for pattern "+"
def action1(self) :
	return Tok(self, "PLUS")
	

# action 2 for pattern "-"
def action2(self) :
	return Tok(self, "MINUS")
	

# action 3 for pattern "*"
def action3(self) :
	return Tok(self, "TIMES")
	

# action 4 for pattern "/"
def action4(self) :
	return Tok(self, "DIVIDE")
	

# action 5 for pattern "="
def action5(self) :
	return Tok(self, "EQUALS")
	

# action 6 for pattern "("
def action6(self) :
	return Tok(self, "LPAREN")
	

# action 7 for pattern ")"
def action7(self) :
	return Tok(self, "RPAREN")
	

# action 8 for pattern "["
def action8(self) :
	return Tok(self, "LSQBRA")
	

# action 9 for pattern "]"
def action9(self) :
	return Tok(self, "RSQBRA")
	

# action 10 for pattern "&"
def action10(self) :
	return Tok(self, "AMP")
	

# action 11 for pattern ">>"
def action11(self) :
	return Tok(self, "SHR")
	

# action 12 for pattern "<<"
def action12(self) :
	return Tok(self, "SHL")
	

# action 13 for pattern "!"
def action13(self) :
	return Tok(self, "NOT")
	

# action 14 for pattern "<"
def action14(self) :
	return Tok(self, "LT")
	

# action 15 for pattern "<="
def action15(self) :
	return Tok(self, "LE")
	

# action 16 for pattern ">"
def action16(self) :
	return Tok(self, "GT")
	

# action 17 for pattern ">="
def action17(self) :
	return Tok(self, "GE")
	

# action 18 for pattern "=="
def action18(self) :
	return Tok(self, "EQ")
	

# action 19 for pattern "!="
def action19(self) :
	return Tok(self, "NE")
	

# action 20 for pattern "&&"
def action20(self) :
	return Tok(self, "AND")
	

# action 21 for pattern "||"
def action21(self) :
	return Tok(self, "OR")
	

# action 22 for pattern "."
def action22(self) :
	return Tok(self, "DOT")
	

# action 23 for pattern "^"
def action23(self) :
	return Tok(self, "CARET")
	

# action 24 for pattern ","
def action24(self) :
	return Tok(self, "COMMA")
	

# action 25 for pattern "{STRING}"
def action25(self) :
	return Tok(self, "STRING")
	
	

# action 26 for pattern "{IDENT}"
def action26(self) :
	token = keywords.get(self.value, "IDENT")
	return Tok(self, token)
	
	

# action 27 for pattern "{HEXNUM}"
def action27(self) :
	self.value = str(str2hex(self.value[2:]))
	return Tok(self, "NUMBER")
	
	

# action 28 for pattern "{DECNUM}"
def action28(self) :
	return Tok(self, "NUMBER")
	
	

# action 29 for pattern "/*[^*]|*[^/]**/"
def action29(self) :
	global lineno
	lineno += self.value.count("\n")
	return
	
	

# action 30 for pattern "\n+"
def action30(self) :
	global lineno
	lineno += 1
	return
	
	

# action 31 for pattern "[[:blank:]]"
def action31(self) :
	return
	
	#        "." :
	#            print "Illegal character '%s'" % self.value
	#            return
	


rows = [ 
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 20, 20, 21, 0, 22, 23, 20, 24],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [26, 26, 26, 26, 27, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 28, 26, 26, 26, 26],
  [0, 0, 0, 0, 0, 29, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 16, 16, 0, 0, 0, 0, 0, 31, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 16, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 32, 33, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 34, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 35, 36, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 37, 37, 0, 0, 0, 37, 37, 37, 0, 0, 0, 0, 37, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 38],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [26, 26, 26, 26, 27, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 28, 26, 26, 26, 26],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 39, 0, 0, 0, 0, 0, 0, 0, 0, 0, 39, 0, 0, 0, 0, 0, 0, 0, 0, 39, 0, 0, 39, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [40, 40, 40, 40, 40, 40, 40, 40, 41, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 42, 42, 0, 0, 0, 42, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 37, 37, 0, 0, 0, 37, 37, 37, 0, 0, 0, 0, 37, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [26, 26, 26, 26, 27, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 28, 26, 26, 26, 26],
  [40, 40, 40, 40, 40, 40, 40, 40, 41, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40],
  [43, 43, 43, 43, 43, 43, 43, 43, 43, 43, 43, 43, 43, 44, 43, 43, 43, 43, 43, 43, 43, 43, 43, 43, 43, 43, 43, 43],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 42, 42, 0, 0, 0, 42, 0, 0, 0, 0, 0, 0, 0, 0],
  [40, 40, 40, 40, 40, 40, 40, 40, 41, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
]
acc = [[], [], [31], [30], [13], [], [10], [6], [7], [3], [1], [24], [2], [22], [4], [28], [28], [14], [5], [16], [26], [8], [9], [23], [], [19], [], [25], [], [20], [], [], [12], [15], [18], [17], [11], [26], [21], [], [], [], [27], [], [29]]
starts = [(1, 1)]
chr2uccl = {'\x00': 0, '\x83': 0, '\x04': 0, '\x87': 0, '\x08': 0, '\x8b': 0, '\x0c': 0, '\x8f': 0, '\x10': 0, '\x93': 0, '\x14': 0, '\x97': 0, '\x18': 0, '\x9b': 0, '\x1c': 0, '\x9f': 0, ' ': 1, '\xa3': 0, '$': 0, '\xa7': 0, '(': 6, '\xab': 0, ',': 10, '\xaf': 0, '0': 14, '\xb3': 0, '4': 15, '\xb7': 0, '8': 15, '\xbb': 0, '<': 16, '\xbf': 0, '@': 0, '\xc3': 0, 'D': 19, '\xc7': 0, 'H': 20, '\xcb': 0, 'L': 20, '\xcf': 0, 'P': 20, '\xd3': 0, 'T': 20, '\xd7': 0, 'X': 21, '\xdb': 0, '\\': 23, '\xdf': 0, '`': 0, '\xe3': 0, 'd': 19, '\xe7': 0, 'h': 20, '\xeb': 0, 'l': 20, '\xef': 0, 'p': 20, '\xf3': 0, 't': 20, '\xf7': 0, 'x': 21, '\xfb': 0, '|': 27, '\xff': 0, '\x80': 0, '\x03': 0, '\x84': 0, '\x07': 0, '\x88': 0, '\x0b': 0, '\x8c': 0, '\x0f': 0, '\x90': 0, '\x13': 0, '\x94': 0, '\x17': 0, '\x98': 0, '\x1b': 0, '\x9c': 0, '\x1f': 0, '\xa0': 0, '#': 0, '\xa4': 0, "'": 0, '\xa8': 0, '+': 9, '\xac': 0, '/': 13, '\xb0': 0, '3': 15, '\xb4': 0, '7': 15, '\xb8': 0, ';': 0, '\xbc': 0, '?': 0, '\xc0': 0, 'C': 19, '\xc4': 0, 'G': 20, '\xc8': 0, 'K': 20, '\xcc': 0, 'O': 20, '\xd0': 0, 'S': 20, '\xd4': 0, 'W': 20, '\xd8': 0, '[': 22, '\xdc': 0, '_': 20, '\xe0': 0, 'c': 19, '\xe4': 0, 'g': 20, '\xe8': 0, 'k': 20, '\xec': 0, 'o': 20, '\xf0': 0, 's': 20, '\xf4': 0, 'w': 20, '\xf8': 0, '{': 0, '\xfc': 0, '\x7f': 0, '\x81': 0, '\x02': 0, '\x85': 0, '\x06': 0, '\x89': 0, '\n': 2, '\x8d': 0, '\x0e': 0, '\x91': 0, '\x12': 0, '\x95': 0, '\x16': 0, '\x99': 0, '\x1a': 0, '\x9d': 0, '\x1e': 0, '\xa1': 0, '"': 4, '\xa5': 0, '&': 5, '\xa9': 0, '*': 8, '\xad': 0, '.': 12, '\xb1': 0, '2': 15, '\xb5': 0, '6': 15, '\xb9': 0, ':': 0, '\xbd': 0, '>': 18, '\xc1': 0, 'B': 19, '\xc5': 0, 'F': 19, '\xc9': 0, 'J': 20, '\xcd': 0, 'N': 20, '\xd1': 0, 'R': 20, '\xd5': 0, 'V': 20, '\xd9': 0, 'Z': 20, '\xdd': 0, '^': 25, '\xe1': 0, 'b': 19, '\xe5': 0, 'f': 19, '\xe9': 0, 'j': 20, '\xed': 0, 'n': 26, '\xf1': 0, 'r': 20, '\xf5': 0, 'v': 20, '\xf9': 0, 'z': 20, '\xfd': 0, '~': 0, '\x01': 0, '\x82': 0, '\x05': 0, '\x86': 0, '\t': 1, '\x8a': 0, '\r': 0, '\x8e': 0, '\x11': 0, '\x92': 0, '\x15': 0, '\x96': 0, '\x19': 0, '\x9a': 0, '\x1d': 0, '\x9e': 0, '!': 3, '\xa2': 0, '%': 0, '\xa6': 0, ')': 7, '\xaa': 0, '-': 11, '\xae': 0, '1': 15, '\xb2': 0, '5': 15, '\xb6': 0, '9': 15, '\xba': 0, '=': 17, '\xbe': 0, 'A': 19, '\xc2': 0, 'E': 19, '\xc6': 0, 'I': 20, '\xca': 0, 'M': 20, '\xce': 0, 'Q': 20, '\xd2': 0, 'U': 20, '\xd6': 0, 'Y': 20, '\xda': 0, ']': 24, '\xde': 0, 'a': 19, '\xe2': 0, 'e': 19, '\xe6': 0, 'i': 20, '\xea': 0, 'm': 20, '\xee': 0, 'q': 20, '\xf2': 0, 'u': 20, '\xf6': 0, 'y': 20, '\xfa': 0, '}': 0, '\xfe': 0}
actions = [None, action1, action2, action3, action4, action5, action6, action7, action8, action9, action10, action11, action12, action13, action14, action15, action16, action17, action18, action19, action20, action21, action22, action23, action24, action25, action26, action27, action28, action29, action30, action31]
eofactions = [None]

lexspec = (rows,acc,starts,actions,eofactions,chr2uccl)

tokens = set([
    'IDENT','NUMBER','STRING',
    'PLUS','MINUS','TIMES','DIVIDE','EQUALS',
    'LPAREN','RPAREN','LSQBRA','RSQBRA','AMP','SHL','SHR',
    'DOT','CARET','COMMA',
    'IF','THEN','ELSE',
    'LT','LE','GT','GE','EQ','NE','NOT'
])

lineno = 1

    #class Tok:
    #    def __init__(self, l, tok):
    #        self.tok = tok
    #       self.lineno = lineno
    #        self.value = l.value

    #   def __hash__(self):
    #       return self.tok.__hash__()

    #   def __eq__(self, other):
    #       if other is None:
    #          return False
    #       else:
    #          if self.tok == other.tok: # and self.value == other.value:
    #             return True
    #          else:
    #             return False

    #   def __ne__(self, other):
    #       return not self.__eq__(other)

    #   def __cmp__(self, other):
    #       return self.tok.__cmp__(other.tok)

    #   def __str__(self):
            #return "Tok(%s,%r,%d)" % (self.tok, self.value, self.lineno)
    #       return self.tok

def Tok(l, str):
    return str

hexdigits = {'0': 0, '1': 1, '2': 2, '3': 3, '4': 4,
             '5': 5, '6': 6, '7': 7, '8': 8, '9': 9,
             'A': 10, 'B': 11, 'C': 12, 'D':13, 'E':14, 'F':15}

def str2hex(str):
    tmp = 0
    str = str.upper()
    for i in str:
        tmp = tmp * 16 + hexdigits[i]
    return tmp

keywords = {
    'if': 'IF',
    'then': 'THEN',
    'else': 'ELSE'
}



