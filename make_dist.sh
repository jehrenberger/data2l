#!/bin/bash

NAME=data2l
VERSION=1.41

###
# Script for .deb package creation
# 
# Settings:
#
#   data2l.sh.debian - starting script to be installed to  /usr/local/bin
#
#   control.debian - name, version, dependencies atd.
#
#   rules.debian - sekce install: what is installed where
#
#   make_dist.sh - the main building csript
#

name=$NAME-$VERSION

#rm -r *.pyo
find . -name '*.pyo' | xargs rm -f
find . -name '*.pyc' | xargs rm -f

echo "Compiling all python sources..."
python -OO -c "import compileall; compileall.compile_dir(\".\", force=1)"

echo "Removing subdir /dist with all its content..."
sudo rm -f -r ./dist

echo "Making subdir /dist/$name..."
mkdir dist
mkdir ./dist/$name

echo "Copying distribution files..."
cp -r ./examples ./lib ./dist/$name
cp -r ./src/* ./dist/$name
mkdir ./dist/$name/bin
cp ./datatool.sh.debian ./dist/$name/bin/data2l
mkdir ./dist/$name/include
cp -r ./include/* ./dist/$name/include
cp ./data2l.desktop.debian ./dist/$name/data2l.desktop


echo "Removing python source files..."
find ./dist -name '*.py' ! -path '*examples*' | xargs rm -f
find ./dist -name '*.pyo' -path '*examples*' | xargs rm -f

echo "Removing subversion meta files..."
find ./dist -path '*/.svn*' | xargs rm -f -r
# files shell not be distributed
rm -rf dist/$name/unit_tests

echo "Packing distribution..."
cd dist/$name
tar cfz $name.tar.gz *

echo "Creating deb..."
dh_make -e opekar@eccam.com -f $name.tar.gz -s

echo "Removing original package..."
rm ../${NAME}_${VERSION}.orig.tar.gz 

echo "Rewriting configure files in /debian directory..."
cp ../../control.debian ./debian/control
cp ../../rules.debian ./debian/rules
cp ../../dirs.debian ./debian/dirs
cp ../../copyright.debian ./debian/copyright
cp ../../datatool.desktop.debian ./debian/data2l.desktop

echo "Building debian package..."
#cd ..
sudo dpkg-buildpackage



