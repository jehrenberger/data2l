#ifndef DATAVIZ_READER_H
#define DATAVIZ_READER_H

#include <cstdio>
#include <cassert>

#include "types.h"


namespace data2l
{

class Reader
    : public virtual StreamBase
{
public:
    virtual ~Reader ()
    { }

    virtual void seek(OffsetType offs) = 0;
    virtual OffsetType tell() = 0;
    virtual void readBin(char *chunk, SizeType num_bytes) = 0;

    virtual void readChar(Char * chunk, SizeType num)
    {
        readBin(reinterpret_cast<char *>(chunk), num);
    }

    virtual void readByte(Byte* chunk, SizeType num)
    {
        readBin(reinterpret_cast<char *>(chunk), num);
    }

    virtual void readByte(Word* chunk, SizeType num)
    {
        readBin(reinterpret_cast<char *>(chunk), num);
    }

    virtual void readByte(DWord* chunk, SizeType num)
    {
        readBin(reinterpret_cast<char *>(chunk), num);
    }

    virtual void readByte(LongLong* chunk, SizeType num)
    {
        readBin(reinterpret_cast<char *>(chunk), num);
    }

    virtual void readByte(Float* chunk, SizeType num)
    {
        readBin(reinterpret_cast<char *>(chunk), num);
    }

    virtual void readWord(Word* chunk, SizeType num)
    {
        readBin(reinterpret_cast<char *>(chunk), num*sizeof(Word));
    }

    virtual void readDWord(DWord* chunk, SizeType num)
    {
        readBin(reinterpret_cast<char *>(chunk), num*sizeof(DWord));
    }

    virtual void readFloat(Float* chunk, SizeType num)
    {
        readBin(reinterpret_cast<char *>(chunk), num*sizeof(Float));
    }
};


} // namespace db

#endif // DATAVIZ_READER_H

// Local Variables:
// mode: C++
// eval: (c-set-style "microsoft")
// End:
