#ifndef DATA2L_DATA2L_H
#define DATA2L_DATA2L_H


#include "types.h"

#include "filereader.h"
#include "filewriter.h"
#include "nullwriter.hpp"

#if defined (WIN32)
#  include "win32sysreader.h"
#  include "win32syswriter.h"
#  include "win32mmapreader.hpp"
#elif defined (__unix__) || defined (__linux__)
#  include "mmapreader.hpp"
#  include "mmapallreader.hpp"
#endif

#include "readert_writert.h"

#include "sax.h"


#endif // DATA2L_DATA2L_H

// Local Variables:
// mode: C++
// eval: (c-set-style "microsoft")
// End:
