#ifndef WIN32SYSWRITER_H
#define WIN32SYSWRITER_H

#include "types.h"
#include "writer.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <share.h>
#include <io.h>


#ifndef W32WCHECK
#  define W32WCHECK(cond, msg, where)                   \
    if (! (cond))                                       \
    {                                                   \
        this->failure = true;                           \
        throw Win32SysWriterException ((msg), errno,    \
            Win32SysWriterException::where, this);      \
    }
#else
#  error W32WCHECK already defined
#endif // W32WCHECK


namespace data2l
{

class Win32SysWriter;
class Win32SysWriterException
    : public StreamException
{
public:
    enum Where {
        errUnspec,
        errOpen,
        errSeek,
        errWrite,
        errClose,
        errTell
    };

    int const error;
    int const where;
    Win32SysWriter * const stream;

    explicit
    Win32SysWriterException (std::string const & err, int eno, Where w,
        Win32SysWriter * s)
        : StreamException (err), error (eno), where (w), stream (s)
    { }
};


class Win32SysWriter
    : public Writer
{
    int file;
    OffsetType filesize;
    OffsetType act_pos;

public:
    Win32SysWriter (char const * filename,
        unsigned oflags = mode::WRONLY | mode::CREAT)
        : act_pos(0)
    {
        modes = oflags;

        int sopen_flags = _O_WRONLY;
        if (oflags & mode::RDONLY)
            sopen_flags |= _O_RDONLY;
        if (oflags & mode::APPEND)
            sopen_flags |= _O_APPEND;
        if (oflags & mode::TRUNC)
            sopen_flags |= _O_TRUNC;
        if (oflags & mode::CREAT)
            sopen_flags |= _O_CREAT;
        if (oflags & mode::EXCL)
            sopen_flags |= _O_EXCL;

        file = _sopen(filename,
            _O_BINARY | _O_RANDOM | _O_WRONLY | sopen_flags,
            _SH_DENYNO, _S_IREAD | _S_IWRITE);
        W32WCHECK(file != -1, "failed to open file", errOpen);
        OffsetType ret;
        ret = _lseeki64(file, 0, SEEK_END);
        W32WCHECK(ret != -1, "failed to seek to end of file", errOpen);
        filesize = ret = _telli64(file);
        W32WCHECK(ret != -1, "_telli64()", errOpen);
        ret = _lseeki64(file, 0, SEEK_SET);
        W32WCHECK(ret != -1, "failed to seek to start of file", errOpen);
    }

    virtual ~Win32SysWriter()
    {
        int ret = _close(file);
        W32WCHECK(ret != -1, "failed to close file", errClose);
    }

    virtual void seek(OffsetType offs)
    {
        OffsetType ret = _lseeki64(file, offs, SEEK_SET);
        W32WCHECK(ret != -1, "faile to seek", errSeek);
        act_pos = offs;
    }

    virtual OffsetType tell()
    {
        return act_pos;
    }

    virtual void writeBin(char const * chunk, size_t num_bytes)
    {
        if (num_bytes == 0)
            return;
        OffsetType o = _telli64(file);
        W32WCHECK(o == act_pos, "offset recorded and real are different",
            errWrite);
        if (this->check_file_boundaries)
            W32WCHECK(act_pos + num_bytes <= filesize, "out of bounds", errWrite);
        OffsetType before = act_pos;
        int bytes_written = _write(file, chunk, num_bytes);
        W32WCHECK(bytes_written != -1, "failed to write chunk", errWrite);
        W32WCHECK(static_cast<size_t>(bytes_written) == num_bytes,
            "failed to write requeted amount", errWrite);
        act_pos = act_pos + num_bytes;
    }
};

} // namespace db


#undef W32WCHECK

#endif // WIN32SYSWRITER_H

// Local Variables:
// mode: C++
// eval: (c-set-style "microsoft")
// End:
